(function(){
    var script = {
 "start": "this.init(); this.visibleComponentsIfPlayerFlagEnabled([this.IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A], 'gyroscopeAvailable'); this.syncPlaylists([this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist,this.mainPlayList]); this.playList_D32EF3CA_C6F4_F365_41E1_7420A01920AF.set('selectedIndex', 0); if(!this.get('fullscreenAvailable')) { [this.IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0,this.IconButton_C945391F_C6F3_FF1B_41E2_5C0A7F52D021].forEach(function(component) { component.set('visible', false); }) }",
 "children": [
  "this.MainViewer",
  "this.Container_EF8F8BD8_E386_8E03_41E3_4CF7CC1F4D8E",
  "this.Container_0DD1BF09_1744_0507_41B3_29434E440055",
  "this.Container_1B9AAD00_16C4_0505_41B5_6F4AE0747E48",
  "this.Container_062AB830_1140_E215_41AF_6C9D65345420",
  "this.Container_23F0F7B8_0C0A_629D_418A_F171085EFBF8",
  "this.Container_39DE87B1_0C06_62AF_417B_8CB0FB5C9D15",
  "this.Container_2F8BB687_0D4F_6B7F_4190_9490D02FBC41",
  "this.Container_2820BA13_0D5D_5B97_4192_AABC38F6F169",
  "this.Container_2A1A5C4D_0D3B_DFF0_41A9_8FC811D03C8E",
  "this.Container_06C41BA5_1140_A63F_41AE_B0CBD78DEFDC"
 ],
 "id": "rootPlayer",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 20,
 "buttonToggleMute": "this.IconButton_EED073D3_E38A_9E06_41E1_6CCC9722545D",
 "backgroundPreloadEnabled": true,
 "definitions": [{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D04B84A8_C6F4_F525_41E6_798E2535DF0D",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 121.5,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Front Courtyard_04",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_9",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_9.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_9_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Pool Area_05",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_26",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_26.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_26_t.jpg",
 "height": 1920
},
{
 "class": "PlayList",
 "items": [
  {
   "class": "PhotoAlbumPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5",
   "player": "this.ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9CPhotoAlbumPlayer",
   "begin": "this.loopAlbum(this.playList_D327C3D5_C6F4_F36F_41E1_A4B0F1A5D753, 0)"
  }
 ],
 "id": "playList_D327C3D5_C6F4_F36F_41E1_A4B0F1A5D753"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1B3950C_C6F4_F4FD_41B4_DAC80AD6C46E",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 109.45,
  "pitch": 0
 }
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Pantry",
 "id": "panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
 "overlays": [
  "this.overlay_D071C49E_C5D5_7F9E_41C7_8737E79C166F",
  "this.overlay_D05D9DB0_C5D7_69A2_41DA_D5400D218477",
  "this.overlay_D28FFC0C_C5D5_AE62_41D4_554B782F2CA1"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -2.77,
   "yaw": 62.09,
   "panorama": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 33.86,
   "yaw": -33.62,
   "panorama": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -12.62,
   "yaw": 11.1,
   "panorama": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_t.jpg"
},
{
 "class": "PlayList",
 "items": [
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 0, 1)",
   "media": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 1, 2)",
   "media": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 2, 3)",
   "media": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 3, 4)",
   "media": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 4, 5)",
   "media": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 5, 6)",
   "media": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 6, 7)",
   "media": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 7, 8)",
   "media": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 8, 9)",
   "media": "this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 9, 10)",
   "media": "this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 10, 11)",
   "media": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 11, 12)",
   "media": "this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 12, 13)",
   "media": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.mainPlayList, 13, 0)",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5",
   "end": "this.trigger('tourEnded')",
   "class": "PhotoAlbumPlayListItem",
   "player": "this.MainViewerPhotoAlbumPlayer"
  }
 ],
 "id": "mainPlayList"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D00924D0_C6F4_F566_41C4_F178FC649B37",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -177.14,
  "pitch": 0
 }
},
{
 "class": "MapPlayer",
 "viewerArea": "this.MapViewer",
 "id": "MapViewerMapPlayer",
 "movementMode": "constrained"
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Master Bathroom",
 "id": "panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D",
 "overlays": [
  "this.overlay_E0C18527_C5D3_5EAE_41BE_3186FEDF405B"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 79.8,
   "yaw": 156.84,
   "panorama": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1924521_C6F4_F727_41DC_F175F1EBFE1C",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -176.77,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Villa Courtyard_03",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_29",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_29.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_29_t.jpg",
 "height": 1920
},
{
 "change": "this.showComponentsWhileMouseOver(this.container_D32B43CD_C6F4_F37F_41DE_DA8BDF805769, [this.htmltext_D32443CE_C6F4_F37D_41AF_4D11F0A48E7D,this.component_D32623D4_C6F4_F36D_41CC_17C0EE8A3F46,this.component_D32603D4_C6F4_F36D_41CD_26243E079EA8], 2000)",
 "class": "PlayList",
 "items": [
  "this.albumitem_D32B53CD_C6F4_F37F_41AD_AF1295CA69A5"
 ],
 "id": "playList_D32923CB_C6F4_F37B_41E1_0802F49BFCB4"
},
{
 "duration": 5000,
 "label": "Kitchen_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_13",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_13.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_13_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Hall Front Courtyard_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_6",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_6.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_6_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Villa Entrance_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_31",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_31.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_31_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D427F570_C6F4_F725_41D6_1B0CD9F28B53",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -91.65,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D087947F_C6F4_F51B_41E0_D5B614464B01",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -0.18,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Pantry_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_20",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_20.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_20_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Main Area_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_10",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_10.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_10_t.jpg",
 "height": 1920
},
{
 "footerHeight": 5,
 "headerBorderColor": "#000000",
 "titlePaddingTop": 5,
 "id": "window_EAC543AF_C5CD_D9BD_41BE_9B029993F938",
 "layout": "vertical",
 "width": 400,
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 5,
 "shadowOpacity": 0.5,
 "headerBackgroundOpacity": 0,
 "bodyPaddingLeft": 0,
 "closeButtonPressedIconColor": "#FFFFFF",
 "paddingLeft": 0,
 "borderSize": 0,
 "verticalAlign": "middle",
 "minHeight": 20,
 "bodyBackgroundColorRatios": [
  0,
  0.5,
  1
 ],
 "backgroundColorRatios": [],
 "closeButtonPressedIconLineWidth": 3,
 "modal": true,
 "closeButtonIconWidth": 20,
 "closeButtonBorderRadius": 11,
 "footerBackgroundColorRatios": [
  0,
  0.9,
  1
 ],
 "backgroundColor": [],
 "hideEffect": {
  "class": "FadeOutEffect",
  "duration": 500,
  "easing": "cubic_in_out"
 },
 "minWidth": 20,
 "closeButtonRollOverBackgroundColor": [],
 "scrollBarColor": "#000000",
 "scrollBarVisible": "rollOver",
 "class": "Window",
 "title": "",
 "gap": 10,
 "height": 600,
 "headerVerticalAlign": "middle",
 "titlePaddingRight": 5,
 "bodyPaddingTop": 0,
 "scrollBarOpacity": 0.5,
 "titlePaddingLeft": 5,
 "titleTextDecoration": "none",
 "titlePaddingBottom": 5,
 "bodyPaddingBottom": 0,
 "closeButtonRollOverBackgroundColorRatios": [
  0
 ],
 "closeButtonBackgroundColor": [],
 "headerPaddingBottom": 5,
 "paddingTop": 0,
 "titleFontSize": "1.29vmin",
 "closeButtonIconHeight": 20,
 "bodyBackgroundColorDirection": "vertical",
 "shadow": true,
 "veilColorRatios": [
  0,
  1
 ],
 "shadowSpread": 1,
 "headerPaddingLeft": 10,
 "titleFontColor": "#000000",
 "veilOpacity": 0.4,
 "footerBackgroundColorDirection": "vertical",
 "veilColor": [
  "#000000",
  "#000000"
 ],
 "children": [
  "this.container_D32B43CD_C6F4_F37F_41DE_DA8BDF805769"
 ],
 "backgroundOpacity": 1,
 "veilColorDirection": "horizontal",
 "bodyBackgroundOpacity": 0,
 "footerBackgroundColor": [
  "#FFFFFF",
  "#EEEEEE",
  "#DDDDDD"
 ],
 "shadowColor": "#000000",
 "veilShowEffect": {
  "class": "FadeInEffect",
  "duration": 500,
  "easing": "cubic_in_out"
 },
 "titleFontWeight": "normal",
 "headerBorderSize": 0,
 "closeButtonRollOverIconColor": "#FFFFFF",
 "paddingRight": 0,
 "closeButtonIconLineWidth": 2,
 "overflow": "scroll",
 "headerBackgroundColorRatios": [
  0,
  0.1,
  1
 ],
 "propagateClick": false,
 "footerBackgroundOpacity": 0,
 "closeButtonPressedBackgroundColor": [],
 "shadowHorizontalLength": 3,
 "headerBackgroundColorDirection": "vertical",
 "headerPaddingRight": 0,
 "bodyBackgroundColor": [
  "#FFFFFF",
  "#DDDDDD",
  "#FFFFFF"
 ],
 "closeButtonPressedBackgroundColorRatios": [
  0
 ],
 "veilHideEffect": {
  "class": "FadeOutEffect",
  "duration": 500,
  "easing": "cubic_in_out"
 },
 "horizontalAlign": "center",
 "backgroundColorDirection": "vertical",
 "titleFontStyle": "normal",
 "shadowVerticalLength": 0,
 "titleFontFamily": "Arial",
 "closeButtonIconColor": "#B2B2B2",
 "scrollBarMargin": 2,
 "closeButtonBackgroundColorRatios": [],
 "data": {
  "name": "Window35666"
 },
 "headerBackgroundColor": [
  "#DDDDDD",
  "#EEEEEE",
  "#FFFFFF"
 ],
 "contentOpaque": false,
 "headerPaddingTop": 10,
 "showEffect": {
  "class": "FadeInEffect",
  "duration": 500,
  "easing": "cubic_in_out"
 },
 "shadowBlurRadius": 6,
 "bodyPaddingRight": 0
},
{
 "duration": 5000,
 "label": "Hall Front Courtyard_03",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_8",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_8.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_8_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0D83421_C6F4_F527_41BE_22DC486ABD33",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "hfov": 120,
  "yaw": 128.1,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1BDD502_C6F4_F4E5_41E5_EB77B94D61EA",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -23.16,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D055F49E_C6F4_F51D_41D9_CA8C490D0D63",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 147.77,
  "pitch": 0
 }
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Front View",
 "overlays": [
  "this.overlay_D57801D7_C5D7_79EE_4184_C4B51CDD2C38",
  "this.overlay_D48973DB_C5DD_D9E6_41E8_0B37EE8D47BD"
 ],
 "id": "panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 56.01,
   "yaw": -51.9,
   "panorama": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -51.83,
   "yaw": 51.77,
   "panorama": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_t.jpg"
},
{
 "duration": 5000,
 "label": "Pool Area_03",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_24",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_24.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_24_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Villa Courtyard",
 "id": "panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
 "overlays": [
  "this.overlay_D18C7915_C5DC_B662_41D1_0018A029F742",
  "this.overlay_D103AE7B_C5D3_AAA6_41E4_5022EEF3A06E",
  "this.overlay_D39FBAF8_C5D7_6BA2_41E4_26A78FFAEF9C",
  "this.overlay_EE6981C7_C5DF_B9ED_41D3_815639E4EFB4"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 2.86,
   "yaw": 61.46,
   "panorama": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 90.55,
   "yaw": -70.55,
   "panorama": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -33.62,
   "yaw": 33.86,
   "panorama": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 59.6,
   "yaw": -27.74,
   "panorama": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1EFD4E4_C6F4_F52D_41E4_D70B0DA6945B",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 146.38,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0B28450_C6F4_F566_41E1_E46ADEE67A6B",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "hfov": 120,
  "yaw": -128.23,
  "pitch": 0
 }
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Master Bedroom",
 "id": "panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180",
 "overlays": [
  "this.overlay_DEF905F0_C5CC_D9A2_41E4_C51DCEA965D9",
  "this.overlay_D9BE452D_C5CF_5EA2_41A9_A8B89BC2AEA7"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 62.09,
   "yaw": -2.77,
   "panorama": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 156.84,
   "yaw": 79.8,
   "panorama": "this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_t.jpg"
},
{
 "class": "PhotoAlbumPlayer",
 "viewerArea": "this.MainViewer",
 "id": "MainViewerPhotoAlbumPlayer",
 "buttonPrevious": [
  "this.IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD",
  "this.IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D",
  "this.IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482"
 ],
 "buttonNext": [
  "this.IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4",
  "this.IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14",
  "this.IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510"
 ]
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0CD8432_C6F4_F525_419E_FA03BDC5210D",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 66.67,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D418F585_C6F4_F7EF_41BC_742ED125377D",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -146.14,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D065C494_C6F4_F5ED_41E5_58E746A3DD5B",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 80.63,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Front Courtyard_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_7",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_7.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_7_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Bathroom",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_0",
 "width": 1080,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_0.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_0_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Master Bedroom_03",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_16",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_16.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_16_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Bedroom",
 "id": "panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0",
 "overlays": [
  "this.overlay_DF0F53FD_C5CD_59A2_41D6_F87C8FC07E6C",
  "this.overlay_D8442E22_C5CD_AAA6_41DC_4ACD969E995A"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -58.5,
   "yaw": 64.17,
   "panorama": "this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -122.43,
   "yaw": 179.82,
   "panorama": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1A38517_C6F4_F4EB_41E5_442F87F2802F",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 88.61,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D416E58F_C6F4_F7FB_41B1_DFF82447FA78",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 167.38,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Main Area_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_11",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_11.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_11_t.jpg",
 "height": 1920
},
{
 "class": "PhotoAlbum",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5",
 "playList": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_AlbumPlayList",
 "label": "Still Images",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_t.png"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Pool Area_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_23",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_23.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_23_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Hall Back Courtyard_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_4",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_4.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_4_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Bathroom",
 "id": "panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9",
 "overlays": [
  "this.overlay_E0346FD2_C5D3_A9E6_41D4_AA5E10CB1F3C"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 64.17,
   "yaw": -58.5,
   "panorama": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_t.jpg"
},
{
 "buttonToggleHotspots": "this.IconButton_EEEB3760_E38B_8603_41D6_FE6B11A3DA96",
 "viewerArea": "this.MainViewer",
 "gyroscopeVerticalDraggingEnabled": true,
 "class": "PanoramaPlayer",
 "mouseControlMode": "drag_rotation",
 "id": "MainViewerPanoramaPlayer",
 "buttonToggleGyroscope": "this.IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A",
 "buttonCardboardView": "this.IconButton_EF7806FA_E38F_8606_41E5_5C4557EBCACB",
 "touchControlMode": "drag_rotation",
 "displayPlaybackBar": true,
 "gyroscopeEnabled": true
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Villa Entrance",
 "id": "panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E",
 "overlays": [
  "this.overlay_D622E8C2_C5DF_57E6_41E2_EB5C216016CD",
  "this.overlay_D1286C43_C5DC_AEE6_41C0_DE150E113840"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 61.46,
   "yaw": 2.86,
   "panorama": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 51.77,
   "yaw": -51.83,
   "panorama": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_t.jpg"
},
{
 "fieldOfViewOverlayInsideColor": "#FFFFFF",
 "height": 1920,
 "fieldOfViewOverlayInsideOpacity": 0.4,
 "label": "Layout Plan",
 "id": "map_CA387849_C6BB_D206_41D3_C00DD57318DC",
 "fieldOfViewOverlayOutsideColor": "#000000",
 "class": "Map",
 "scaleMode": "fit_inside",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/map_CA387849_C6BB_D206_41D3_C00DD57318DC.png",
    "width": 1920,
    "height": 1920
   },
   {
    "class": "ImageResourceLevel",
    "url": "media/map_CA387849_C6BB_D206_41D3_C00DD57318DC_lq.png",
    "tags": "preload",
    "width": 256,
    "height": 256
   }
  ]
 },
 "maximumZoomFactor": 1.2,
 "minimumZoomFactor": 0.5,
 "width": 1920,
 "fieldOfViewOverlayRadiusScale": 0.3,
 "fieldOfViewOverlayOutsideOpacity": 0,
 "thumbnailUrl": "media/map_CA387849_C6BB_D206_41D3_C00DD57318DC_t.png",
 "initialZoomFactor": 1
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "hfov": 120,
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Back Courtyard_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_3",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_3.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_3_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Pool Area_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_22",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_22.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_22_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0E96411_C6F4_F4E7_41E4_9A8FD4961B52",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -100.2,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Main Area_03",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_12",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_12.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_12_t.jpg",
 "height": 1920
},
{
 "class": "PhotoAlbumPlayer",
 "viewerArea": "this.ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9C",
 "id": "ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9CPhotoAlbumPlayer",
 "buttonPrevious": [
  "this.IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD",
  "this.IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D",
  "this.IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482"
 ],
 "buttonNext": [
  "this.IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4",
  "this.IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14",
  "this.IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510"
 ]
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "class": "PlayList",
 "items": [
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 0, 1)",
   "media": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 1, 2)",
   "media": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 2, 3)",
   "media": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 3, 4)",
   "media": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 4, 5)",
   "media": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 5, 6)",
   "media": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 6, 7)",
   "media": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 7, 8)",
   "media": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 8, 9)",
   "media": "this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 9, 10)",
   "media": "this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 10, 11)",
   "media": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 11, 12)",
   "media": "this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_camera"
  },
  {
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 12, 13)",
   "media": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299",
   "class": "PanoramaPlayListItem",
   "player": "this.MainViewerPanoramaPlayer",
   "camera": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_camera"
  },
  {
   "class": "PhotoAlbumPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5",
   "player": "this.MainViewerPhotoAlbumPlayer",
   "begin": "this.setEndToItemIndex(this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist, 13, 0)"
  }
 ],
 "id": "ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D41C457A_C6F4_F725_41D6_1038FAD6CDA0",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 177.23,
  "pitch": 0
 }
},
{
 "class": "PlayList",
 "items": [
  {
   "class": "MapPlayListItem",
   "media": "this.map_CA387849_C6BB_D206_41D3_C00DD57318DC",
   "player": "this.MapViewerMapPlayer",
   "begin": "this.MapViewerMapPlayer.set('movementMode', 'free_drag_and_rotation')"
  }
 ],
 "id": "playList_D32EF3CA_C6F4_F365_41E1_7420A01920AF"
},
{
 "duration": 5000,
 "label": "Villa Courtyard_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_27",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_27.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_27_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0BD3441_C6F4_F567_41B7_3D0666A80FBD",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -118.54,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Pool Area_04",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_25",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_25.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_25_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1FFC4DB_C6F4_F51B_41E8_54658EE21FD3",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -89.45,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Hall Entrance",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_5",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_5.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_5_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Bedroom",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_1",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_1.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_1_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Musholla",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_19",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_19.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_19_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Hall Main Area",
 "id": "panorama_CEF54088_C5CF_5662_41DA_C2352BB20299",
 "overlays": [
  "this.overlay_D99058AF_C5F4_B7BE_4187_9AC8F1148FDC",
  "this.overlay_D89A713C_C5F7_B6A3_41DB_D9099C3CB357"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -99.37,
   "yaw": 88.35,
   "panorama": "this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -32.23,
   "yaw": -91.39,
   "panorama": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_t.jpg"
},
{
 "duration": 5000,
 "label": "Kitchen_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_14",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_14.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_14_t.jpg",
 "height": 1920
},
{
 "duration": 5000,
 "label": "Front View",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_2",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_2.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_2_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Hall Entrance",
 "id": "panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5",
 "overlays": [
  "this.overlay_D70485AC_C5DC_B9A2_41E4_F754FBFB89FC",
  "this.overlay_D6931C52_C5DF_EEE7_41CC_942AF1E9953A"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -51.9,
   "yaw": 56.01,
   "panorama": "this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -113.33,
   "yaw": 3.23,
   "panorama": "this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_t.jpg"
},
{
 "duration": 5000,
 "label": "Villa Entrance",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_30",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_30.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_30_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D091A470_C6F4_F525_4195_68A4B96F0EA4",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -168.9,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D0A22460_C6F4_F526_41E8_17BC28C48D2B",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 152.26,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Master Bedroom_01",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_17",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_17.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_17_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1DE54EE_C6F4_F53D_41D8_3329A29B6618",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -120.4,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Master Bathroom",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_15",
 "width": 1080,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_15.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_15_t.jpg",
 "height": 1350
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D03BE4B2_C6F4_F525_41E0_FBDBD3167071",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 57.57,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D077048A_C6F4_F5E5_41E6_F7802A9DA409",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -115.83,
  "pitch": 0
 }
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Hall Front Courtyard",
 "id": "panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866",
 "overlays": [
  "this.overlay_D9C33A3D_C5F3_AA9D_41BF_DA2CBE43A675",
  "this.overlay_D962094E_C5F3_D6FE_41E2_E5CBD0D4A80D",
  "this.overlay_D9156281_C5F3_5A62_41E5_A65E9FCB0F66"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -70.55,
   "yaw": 90.55,
   "panorama": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -91.39,
   "yaw": -32.23,
   "panorama": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 3.23,
   "yaw": -113.33,
   "panorama": "this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D02B44BD_C6F4_F51F_41E8_D56CCC11746F",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -123.99,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Pantry_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_21",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_21.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_21_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Hall Back Courtyard",
 "id": "panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6",
 "overlays": [
  "this.overlay_D89E003E_C5F4_B69E_41DC_FD918FB32892"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 88.35,
   "yaw": -99.37,
   "panorama": "this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D1CDD4F9_C6F4_F527_41E2_383550885407",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": -117.91,
  "pitch": 0
 }
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Master Bedroom_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_18",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_18.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_18_t.jpg",
 "height": 1920
},
{
 "hfov": 360,
 "hfovMin": "150%",
 "class": "Panorama",
 "label": "Pool Area",
 "id": "panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30",
 "overlays": [
  "this.overlay_D26CFFBF_C5D4_A99E_41DC_44D974D41F81",
  "this.overlay_DCB5EA2F_C5D3_6ABE_41B5_42913216DFC3",
  "this.overlay_DD1367E9_C5D3_79A2_41DB_116CCE6C83FF"
 ],
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "backwardYaw": -27.74,
   "yaw": 59.6,
   "panorama": "this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 11.1,
   "yaw": -12.62,
   "panorama": "this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59",
   "distance": 1
  },
  {
   "class": "AdjacentPanorama",
   "backwardYaw": 179.82,
   "yaw": -122.43,
   "panorama": "this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0",
   "distance": 1
  }
 ],
 "vfov": 180,
 "hfovMax": 150,
 "partial": false,
 "frames": [
  {
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/b/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/b/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/b/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/f/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/f/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/f/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/l/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/l/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/l/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/u/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/u/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/u/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/r/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/r/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/r/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/d/0/{row}_{column}.jpg",
      "width": 1536,
      "rowCount": 3,
      "colCount": 3,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/d/1/{row}_{column}.jpg",
      "width": 1024,
      "rowCount": 2,
      "colCount": 2,
      "class": "TiledImageResourceLevel",
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_0/d/2/{row}_{column}.jpg",
      "width": 512,
      "rowCount": 1,
      "colCount": 1,
      "class": "TiledImageResourceLevel",
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_t.jpg"
  }
 ],
 "thumbnailUrl": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_t.jpg"
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "camera_D01914C6_C6F4_F56D_41DE_11B10D42CE80",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 128.17,
  "pitch": 0
 }
},
{
 "duration": 5000,
 "label": "Villa Courtyard_02",
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_28",
 "width": 1920,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "class": "ImageResourceLevel",
    "url": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_28.jpg"
   }
  ]
 },
 "class": "Photo",
 "thumbnailUrl": "media/album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_28_t.jpg",
 "height": 1920
},
{
 "automaticZoomSpeed": 10,
 "initialSequence": {
  "class": "PanoramaCameraSequence",
  "movements": [
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_in",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "linear",
    "yawDelta": 323,
    "yawSpeed": 7.96
   },
   {
    "class": "DistancePanoramaCameraMovement",
    "easing": "cubic_out",
    "yawDelta": 18.5,
    "yawSpeed": 7.96
   }
  ],
  "restartMovementOnUserInteraction": false
 },
 "class": "PanoramaCamera",
 "id": "panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_camera",
 "initialPosition": {
  "class": "PanoramaCameraPosition",
  "yaw": 0,
  "pitch": 0
 }
},
{
 "toolTipPaddingTop": 7,
 "progressBorderRadius": 0,
 "id": "MainViewer",
 "left": 0,
 "width": "100%",
 "paddingBottom": 0,
 "toolTipPaddingRight": 10,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "toolTipDisplayTime": 600,
 "toolTipPaddingLeft": 10,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "playbackBarHeadHeight": 15,
 "minHeight": 50,
 "playbackBarBottom": 5,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBorderColor": "#FFFFFF",
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 100,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 0.5,
 "height": "100%",
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": 13,
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 7,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 0,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": true,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Georgia",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "top": 0,
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 2000,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 55,
 "toolTipBackgroundColor": "#000000",
 "toolTipFontColor": "#FFFFFF",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "Main Viewer"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "children": [
  "this.Container_EF8F8BD8_E386_8E02_41E5_FC5C5513733A",
  "this.Container_EF8F8BD8_E386_8E02_41E5_90850B5F0BBE"
 ],
 "backgroundOpacity": 0,
 "id": "Container_EF8F8BD8_E386_8E03_41E3_4CF7CC1F4D8E",
 "width": 115.05,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingBottom": 0,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "height": 641,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--SETTINGS"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Label_0DD14F09_1744_0507_41AA_D8475423214A",
  "this.Label_0DD1AF09_1744_0507_41B4_9F5A60B503B2"
 ],
 "backgroundOpacity": 0,
 "id": "Container_0DD1BF09_1744_0507_41B3_29434E440055",
 "left": 30,
 "width": 573,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "overflow": "visible",
 "top": 15,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "height": 133,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--STICKER"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Image_1B99DD00_16C4_0505_41B3_51F09727447A",
  "this.Container_1B99BD00_16C4_0505_41A4_A3C2452B0288",
  "this.IconButton_C945391F_C6F3_FF1B_41E2_5C0A7F52D021"
 ],
 "id": "Container_1B9AAD00_16C4_0505_41B5_6F4AE0747E48",
 "left": "0%",
 "backgroundImageUrl": "skin/Container_1B9AAD00_16C4_0505_41B5_6F4AE0747E48.png",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingBottom": 0,
 "backgroundOpacity": 0.64,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "verticalAlign": "top",
 "overflow": "visible",
 "bottom": 0,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "height": 118,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--MENU"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Container_062A782F_1140_E20B_41AF_B3E5DE341773",
  "this.Container_062A9830_1140_E215_41A7_5F2BBE5C20E4"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_062AB830_1140_E215_41AF_6C9D65345420",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_062AB830_1140_E215_41AF_6C9D65345420, false, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--INFO photo"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_23F7B7B7_0C0A_6293_4197_F931EEC6FA48",
  "this.Container_23F097B8_0C0A_629D_4176_D87C90BA32B6"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_23F0F7B8_0C0A_629D_418A_F171085EFBF8",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_23F0F7B8_0C0A_629D_418A_F171085EFBF8, false, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--INFO photoalbum"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_39A197B1_0C06_62AF_419A_D15E4DDD2528"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_39DE87B1_0C06_62AF_417B_8CB0FB5C9D15",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_39DE87B1_0C06_62AF_417B_8CB0FB5C9D15, false, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--PANORAMA LIST"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_2F8A6686_0D4F_6B71_4174_A02FE43588D3"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_2F8BB687_0D4F_6B7F_4190_9490D02FBC41",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_2F8BB687_0D4F_6B7F_4190_9490D02FBC41, false, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--FLOORPLAN"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_28215A13_0D5D_5B97_4198_A7CA735E9E0A"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_2820BA13_0D5D_5B97_4192_AABC38F6F169",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_2820BA13_0D5D_5B97_4192_AABC38F6F169, true, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--PHOTOALBUM + text"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_2A193C4C_0D3B_DFF0_4161_A2CD128EF536"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_2A1A5C4D_0D3B_DFF0_41A9_8FC811D03C8E",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "click": "this.setComponentVisibility(this.Container_2A1A5C4D_0D3B_DFF0_41A9_8FC811D03C8E, false, 0, null, null, false)",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--PHOTOALBUM"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "children": [
  "this.Container_06C5DBA5_1140_A63F_41AD_1D83A33F1255",
  "this.Container_06C43BA5_1140_A63F_41A1_96DC8F4CAD2F"
 ],
 "backgroundOpacity": 0.6,
 "id": "Container_06C41BA5_1140_A63F_41AE_B0CBD78DEFDC",
 "left": "0%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "creationPolicy": "inAdvance",
 "verticalAlign": "top",
 "overflow": "scroll",
 "top": "0%",
 "bottom": "0%",
 "scrollBarColor": "#04A3E1",
 "minWidth": 1,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "--REALTOR"
 },
 "shadow": false,
 "layout": "absolute",
 "visible": false
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EED073D3_E38A_9E06_41E1_6CCC9722545D",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "toggle",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_EED073D3_E38A_9E06_41E1_6CCC9722545D_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton MUTE"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EED073D3_E38A_9E06_41E1_6CCC9722545D.png",
 "cursor": "hand"
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "toggle",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton FULLSCREEN"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0.png",
 "cursor": "hand"
},
{
 "cursor": "hand",
 "maxHeight": 128,
 "backgroundOpacity": 0,
 "id": "IconButton_C945391F_C6F3_FF1B_41E2_5C0A7F52D021",
 "width": 56,
 "paddingBottom": 0,
 "maxWidth": 128,
 "borderRadius": 0,
 "right": "1%",
 "toolTipFontStyle": "normal",
 "toolTipPaddingLeft": 6,
 "toolTipShadowOpacity": 1,
 "toolTipPaddingRight": 6,
 "paddingRight": 0,
 "toolTipDisplayTime": 600,
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "toolTipTextShadowOpacity": 0,
 "minHeight": 1,
 "toolTipFontFamily": "Arial",
 "toolTip": "Fullscreen",
 "verticalAlign": "middle",
 "toolTipPaddingTop": 4,
 "toolTipShadowHorizontalLength": 0,
 "toolTipShadowSpread": 0,
 "bottom": "9.32%",
 "toolTipBorderColor": "#767676",
 "minWidth": 1,
 "mode": "toggle",
 "height": 34,
 "class": "IconButton",
 "horizontalAlign": "center",
 "toolTipShadowVerticalLength": 0,
 "toolTipOpacity": 1,
 "toolTipFontSize": 12,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "toolTipShadowBlurRadius": 3,
 "toolTipTextShadowColor": "#000000",
 "transparencyActive": true,
 "toolTipTextShadowBlurRadius": 3,
 "paddingTop": 0,
 "data": {
  "name": "IconButton1493"
 },
 "toolTipPaddingBottom": 4,
 "shadow": false,
 "toolTipFontWeight": "normal",
 "iconURL": "skin/IconButton_C945391F_C6F3_FF1B_41E2_5C0A7F52D021.png",
 "toolTipBorderSize": 1,
 "toolTipShadowColor": "#333333"
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC, this.camera_D418F585_C6F4_F7EF_41BC_742ED125377D); this.mainPlayList.set('selectedIndex', 3)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -33.62,
   "hfov": 14.52,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0_HS_0_0_0_map.gif",
      "width": 61,
      "height": 16
     }
    ]
   },
   "pitch": -10.45
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_D31F43D8_C6F4_F365_41E3_8A043F0450C3",
   "yaw": -33.62,
   "pitch": -10.45,
   "distance": 100,
   "hfov": 14.52
  }
 ],
 "id": "overlay_D071C49E_C5D5_7F9E_41C7_8737E79C166F",
 "data": {
  "label": "Circle Arrow 03c"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180, this.camera_D41C457A_C6F4_F725_41D6_1038FAD6CDA0); this.mainPlayList.set('selectedIndex', 7)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 62.09,
   "hfov": 7.25,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_1_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -3.92
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B2EB62_C5F5_AAA6_41E7_741F7D4BF8C5",
   "yaw": 62.09,
   "pitch": -3.92,
   "distance": 100,
   "hfov": 7.25
  }
 ],
 "id": "overlay_D05D9DB0_C5D7_69A2_41DA_D5400D218477",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30, this.camera_D416E58F_C6F4_F7FB_41B1_DFF82447FA78); this.mainPlayList.set('selectedIndex', 5)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 11.1,
   "hfov": 16.93,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0_HS_2_0_0_map.gif",
      "width": 61,
      "height": 16
     }
    ]
   },
   "pitch": -11.25
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_D31FB3D8_C6F4_F365_41D5_5E35DA96E2B3",
   "yaw": 11.1,
   "pitch": -11.25,
   "distance": 100,
   "hfov": 16.93
  }
 ],
 "id": "overlay_D28FFC0C_C5D5_AE62_41D4_554B782F2CA1",
 "data": {
  "label": "Circle Arrow 03c"
 },
 "rollOverDisplay": false
},
{
 "progressBorderRadius": 0,
 "toolTipPaddingRight": 6,
 "id": "MapViewer",
 "width": "100%",
 "paddingBottom": 0,
 "toolTipPaddingTop": 4,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "toolTipDisplayTime": 600,
 "toolTipPaddingLeft": 6,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "playbackBarHeadHeight": 15,
 "minHeight": 1,
 "playbackBarBottom": 0,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBorderColor": "#FFFFFF",
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 1,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 1,
 "height": "100%",
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": 12,
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "show": "this.setMediaBehaviour(this.playList_D32EF3CA_C6F4_F365_41E1_7420A01920AF, 0)",
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "toolTipShadowHorizontalLength": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": false,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 2000,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "toolTipShadowVerticalLength": 0,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 2,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "Floor Plan"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180, this.camera_D0E96411_C6F4_F4E7_41E4_9A8FD4961B52); this.mainPlayList.set('selectedIndex', 7)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 156.84,
   "hfov": 17.51,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -4.93
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F5512601_C5D4_DA62_41E6_83BC492D6B25",
   "yaw": 156.84,
   "pitch": -4.93,
   "distance": 50,
   "hfov": 17.51
  }
 ],
 "id": "overlay_E0C18527_C5D3_5EAE_41BE_3186FEDF405B",
 "data": {
  "label": "Circle Arrow 05 Left"
 },
 "rollOverDisplay": false
},
{
 "begin": "this.updateMediaLabelFromPlayList(this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_AlbumPlayList, this.htmltext_D32443CE_C6F4_F37D_41AF_4D11F0A48E7D, this.albumitem_D32B53CD_C6F4_F37F_41AD_AF1295CA69A5); this.loopAlbum(this.playList_D32923CB_C6F4_F37B_41E1_0802F49BFCB4, 0)",
 "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5",
 "class": "PhotoAlbumPlayListItem",
 "player": "this.viewer_uidD32AC3CC_C6F4_F37D_41BB_1BA4830B6B48PhotoAlbumPlayer",
 "id": "albumitem_D32B53CD_C6F4_F37F_41AD_AF1295CA69A5"
},
{
 "children": [
  "this.viewer_uidD32AC3CC_C6F4_F37D_41BB_1BA4830B6B48",
  {
   "children": [
    "this.htmltext_D32443CE_C6F4_F37D_41AF_4D11F0A48E7D"
   ],
   "scrollBarWidth": 7,
   "left": 0,
   "backgroundOpacity": 0.3,
   "borderRadius": 0,
   "right": 0,
   "paddingBottom": 0,
   "paddingRight": 0,
   "paddingLeft": 0,
   "borderSize": 0,
   "propagateClick": false,
   "minHeight": 20,
   "backgroundColorRatios": [],
   "verticalAlign": "bottom",
   "bottom": 0,
   "scrollBarColor": "#FFFFFF",
   "minWidth": 20,
   "backgroundColor": [],
   "scrollBarVisible": "rollOver",
   "class": "Container",
   "horizontalAlign": "left",
   "gap": 10,
   "backgroundColorDirection": "vertical",
   "scrollBarOpacity": 0.5,
   "overflow": "scroll",
   "scrollBarMargin": 2,
   "height": "30%",
   "contentOpaque": true,
   "paddingTop": 0,
   "data": {
    "name": "Container7093"
   },
   "shadow": false,
   "layout": "vertical"
  },
  "this.component_D32623D4_C6F4_F36D_41CC_17C0EE8A3F46",
  "this.component_D32603D4_C6F4_F36D_41CD_26243E079EA8"
 ],
 "backgroundOpacity": 0.3,
 "id": "container_D32B43CD_C6F4_F37F_41DE_DA8BDF805769",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 20,
 "backgroundColorRatios": [],
 "scrollBarColor": "#000000",
 "minWidth": 20,
 "backgroundColor": [],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container7092"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5, this.camera_D02B44BD_C6F4_F51F_41E8_D56CCC11746F); this.mainPlayList.set('selectedIndex', 1)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -51.9,
   "hfov": 7.85,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0_HS_0_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -12.4
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F56C65FA_C5D4_D9A6_41D1_7ABDB7E2802C",
   "yaw": -51.9,
   "pitch": -12.4,
   "distance": 100,
   "hfov": 7.85
  }
 ],
 "id": "overlay_D57801D7_C5D7_79EE_4184_C4B51CDD2C38",
 "data": {
  "label": "Circle Arrow 02b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E, this.camera_D01914C6_C6F4_F56D_41DE_11B10D42CE80); this.mainPlayList.set('selectedIndex', 2)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 51.77,
   "hfov": 7.12,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0_HS_1_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -10.09
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F56DA5FA_C5D4_D9A6_41C6_32B4DA3E097C",
   "yaw": 51.77,
   "pitch": -10.09,
   "distance": 100,
   "hfov": 7.12
  }
 ],
 "id": "overlay_D48973DB_C5DD_D9E6_41E8_0B37EE8D47BD",
 "data": {
  "label": "Circle Arrow 02b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59, this.camera_D1EFD4E4_C6F4_F52D_41E4_D70B0DA6945B); this.mainPlayList.set('selectedIndex', 4)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 33.86,
   "hfov": 6.27,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_0_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -2.08
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F50A3206_C5DD_5A6E_41BB_CB6B1A590729",
   "yaw": 33.86,
   "pitch": -2.08,
   "distance": 100,
   "hfov": 6.27
  }
 ],
 "id": "overlay_D18C7915_C5DC_B662_41D1_0018A029F742",
 "data": {
  "label": "Circle Arrow 06"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30, this.camera_D1DE54EE_C6F4_F53D_41D8_3329A29B6618); this.mainPlayList.set('selectedIndex', 5)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -27.74,
   "hfov": 6.43,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -1.43
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F50A7206_C5DD_5A6E_41C0_F87D2AB805E1",
   "yaw": -27.74,
   "pitch": -1.43,
   "distance": 100,
   "hfov": 6.43
  }
 ],
 "id": "overlay_D103AE7B_C5D3_AAA6_41E4_5022EEF3A06E",
 "data": {
  "label": "Circle Arrow 06"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866, this.camera_D1FFC4DB_C6F4_F51B_41E8_54658EE21FD3); this.mainPlayList.set('selectedIndex', 10)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -70.55,
   "hfov": 8.89,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_2_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": 1.34
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F50AD206_C5DD_5A6E_41E2_4F2DFB872F5C",
   "yaw": -70.55,
   "pitch": 1.34,
   "distance": 100,
   "hfov": 8.89
  }
 ],
 "id": "overlay_D39FBAF8_C5D7_6BA2_41E4_26A78FFAEF9C",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E, this.camera_D00924D0_C6F4_F566_41C4_F178FC649B37); this.mainPlayList.set('selectedIndex', 2)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 61.46,
   "hfov": 8.51,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_3_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -1.69
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F5092207_C5DD_5A6E_41D1_924F0D278D98",
   "yaw": 61.46,
   "pitch": -1.69,
   "distance": 100,
   "hfov": 8.51
  }
 ],
 "id": "overlay_EE6981C7_C5DF_B9ED_41D3_815639E4EFB4",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59, this.camera_D1CDD4F9_C6F4_F527_41E2_383550885407); this.mainPlayList.set('selectedIndex', 4)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -2.77,
   "hfov": 9.52,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_1_HS_0_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -1.48
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B71B63_C5F5_AAA6_41E2_AEF5F1C3393C",
   "yaw": -2.77,
   "pitch": -1.48,
   "distance": 100,
   "hfov": 9.52
  }
 ],
 "id": "overlay_DEF905F0_C5CC_D9A2_41E4_C51DCEA965D9",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D, this.camera_D1BDD502_C6F4_F4E5_41E5_EB77B94D61EA); this.mainPlayList.set('selectedIndex', 8)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 79.8,
   "hfov": 7.27,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_1_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -1.14
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B74B63_C5F5_AAA6_41D0_EE1782E25501",
   "yaw": 79.8,
   "pitch": -1.14,
   "distance": 100,
   "hfov": 7.27
  }
 ],
 "id": "overlay_D9BE452D_C5CF_5EA2_41A9_A8B89BC2AEA7",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "maxHeight": 150,
 "id": "IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD",
 "width": "12%",
 "backgroundOpacity": 0,
 "maxWidth": 150,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 70,
 "minWidth": 70,
 "mode": "push",
 "height": "8%",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton <"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD.png",
 "cursor": "hand"
},
{
 "cursor": "hand",
 "maxHeight": 60,
 "id": "IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D",
 "left": 10,
 "width": "14.22%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "top": "20%",
 "bottom": "20%",
 "minWidth": 50,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D_pressed.png",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton <"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D.png"
},
{
 "cursor": "hand",
 "maxHeight": 60,
 "id": "IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482",
 "left": 10,
 "width": "14.22%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "top": "20%",
 "bottom": "20%",
 "minWidth": 50,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482_pressed.png",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton <"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482.png"
},
{
 "maxHeight": 150,
 "id": "IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4",
 "width": "12%",
 "backgroundOpacity": 0,
 "maxWidth": 150,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 70,
 "minWidth": 70,
 "mode": "push",
 "height": "8%",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton >"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4.png",
 "cursor": "hand"
},
{
 "cursor": "hand",
 "maxHeight": 60,
 "backgroundOpacity": 0,
 "id": "IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14",
 "width": "14.22%",
 "paddingBottom": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 10,
 "rollOverIconURL": "skin/IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14_rollover.png",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "middle",
 "top": "20%",
 "bottom": "20%",
 "minWidth": 50,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14_pressed.png",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton >"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14.png"
},
{
 "cursor": "hand",
 "maxHeight": 60,
 "backgroundOpacity": 0,
 "id": "IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510",
 "width": "14.22%",
 "paddingBottom": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 10,
 "rollOverIconURL": "skin/IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510_rollover.png",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "middle",
 "top": "20%",
 "bottom": "20%",
 "minWidth": 50,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510_pressed.png",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton >"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510.png"
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9, this.camera_D04B84A8_C6F4_F525_41E6_798E2535DF0D); this.mainPlayList.set('selectedIndex', 9)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 64.17,
   "hfov": 7.27,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_1_HS_0_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -0.61
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B04B63_C5F5_AAA6_41D3_35A9ABC49322",
   "yaw": 64.17,
   "pitch": -0.61,
   "distance": 100,
   "hfov": 7.27
  }
 ],
 "id": "overlay_DF0F53FD_C5CD_59A2_41D6_F87C8FC07E6C",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30, this.camera_D03BE4B2_C6F4_F525_41E0_FBDBD3167071); this.mainPlayList.set('selectedIndex', 5)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 179.82,
   "hfov": 7.26,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_1_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -2.7
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B0FB63_C5F5_AAA6_41DB_0D69E2C8C80E",
   "yaw": 179.82,
   "pitch": -2.7,
   "distance": 100,
   "hfov": 7.26
  }
 ],
 "id": "overlay_D8442E22_C5CD_AAA6_41DC_4ACD969E995A",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "class": "PhotoPlayList",
 "items": [
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_2",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.35",
     "zoomFactor": 1.1,
     "y": "0.57"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_5",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.46",
     "zoomFactor": 1.1,
     "y": "0.28"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_31",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.62",
     "zoomFactor": 1.1,
     "y": "0.65"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_30",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.64",
     "zoomFactor": 1.1,
     "y": "0.54"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_27",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.57",
     "zoomFactor": 1.1,
     "y": "0.49"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_28",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.25",
     "zoomFactor": 1.1,
     "y": "0.71"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_29",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.56",
     "zoomFactor": 1.1,
     "y": "0.46"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_20",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.54",
     "zoomFactor": 1.1,
     "y": "0.50"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_21",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.30",
     "zoomFactor": 1.1,
     "y": "0.67"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_22",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.63",
     "zoomFactor": 1.1,
     "y": "0.52"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_23",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.54",
     "zoomFactor": 1.1,
     "y": "0.65"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_24",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.75",
     "zoomFactor": 1.1,
     "y": "0.37"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_25",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.41",
     "zoomFactor": 1.1,
     "y": "0.26"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_26",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.65",
     "zoomFactor": 1.1,
     "y": "0.71"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_17",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.63",
     "zoomFactor": 1.1,
     "y": "0.55"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_18",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.36",
     "zoomFactor": 1.1,
     "y": "0.65"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_16",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.68",
     "zoomFactor": 1.1,
     "y": "0.27"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_15",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.51",
     "zoomFactor": 1.1,
     "y": "0.54"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_1",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.51",
     "zoomFactor": 1.1,
     "y": "0.59"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_0",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.39",
     "zoomFactor": 1.1,
     "y": "0.49"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_6",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.60",
     "zoomFactor": 1.1,
     "y": "0.29"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_7",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.71",
     "zoomFactor": 1.1,
     "y": "0.49"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_8",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.54",
     "zoomFactor": 1.1,
     "y": "0.62"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_9",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.43",
     "zoomFactor": 1.1,
     "y": "0.29"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_10",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.52",
     "zoomFactor": 1.1,
     "y": "0.31"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_11",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1.1,
     "y": "0.36"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_12",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.39",
     "zoomFactor": 1.1,
     "y": "0.55"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_3",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.55",
     "zoomFactor": 1.1,
     "y": "0.56"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_4",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.60",
     "zoomFactor": 1.1,
     "y": "0.70"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_19",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.55",
     "zoomFactor": 1.1,
     "y": "0.59"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_13",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.63",
     "zoomFactor": 1.1,
     "y": "0.72"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  },
  {
   "class": "PhotoPlayListItem",
   "media": "this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_14",
   "camera": {
    "duration": 5000,
    "targetPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.37",
     "zoomFactor": 1.1,
     "y": "0.40"
    },
    "class": "MovementPhotoCamera",
    "easing": "linear",
    "scaleMode": "fit_outside",
    "initialPosition": {
     "class": "PhotoCameraPosition",
     "x": "0.50",
     "zoomFactor": 1,
     "y": "0.50"
    }
   }
  }
 ],
 "id": "album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_AlbumPlayList"
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0, this.camera_D077048A_C6F4_F5E5_41E6_F7802A9DA409); this.mainPlayList.set('selectedIndex', 6)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -58.5,
   "hfov": 17.48,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -6.14
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_F556C601_C5D4_DA62_41D2_745B0780B408",
   "yaw": -58.5,
   "pitch": -6.14,
   "distance": 50,
   "hfov": 17.48
  }
 ],
 "id": "overlay_E0346FD2_C5D3_A9E6_41D4_AA5E10CB1F3C",
 "data": {
  "label": "Circle Arrow 05 Left"
 },
 "rollOverDisplay": false
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EEEB3760_E38B_8603_41D6_FE6B11A3DA96",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "toggle",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_EEEB3760_E38B_8603_41D6_FE6B11A3DA96_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton HS "
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EEEB3760_E38B_8603_41D6_FE6B11A3DA96.png",
 "cursor": "hand"
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "toggle",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A_pressed.png",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton GYRO"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A.png",
 "cursor": "hand"
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EF7806FA_E38F_8606_41E5_5C4557EBCACB",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "rollOverIconURL": "skin/IconButton_EF7806FA_E38F_8606_41E5_5C4557EBCACB_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton VR"
 },
 "shadow": false,
 "visible": false,
 "iconURL": "skin/IconButton_EF7806FA_E38F_8606_41E5_5C4557EBCACB.png",
 "cursor": "hand"
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2, this.camera_D0B28450_C6F4_F566_41E1_E46ADEE67A6B); this.mainPlayList.set('selectedIndex', 0)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -51.83,
   "hfov": 9.67,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_1_HS_0_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -8.12
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0ACDB61_C5F5_AAA2_41C7_82B03F58FDBA",
   "yaw": -51.83,
   "pitch": -8.12,
   "distance": 100,
   "hfov": 9.67
  }
 ],
 "id": "overlay_D622E8C2_C5DF_57E6_41E2_EB5C216016CD",
 "data": {
  "label": "Circle Arrow 02b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC, this.camera_D0BD3441_C6F4_F567_41B7_3D0666A80FBD); this.mainPlayList.set('selectedIndex', 3)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 2.86,
   "hfov": 8.81,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_1_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -0.88
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B30B61_C5F5_AAA2_41DA_A67FDA4E68E6",
   "yaw": 2.86,
   "pitch": -0.88,
   "distance": 100,
   "hfov": 8.81
  }
 ],
 "id": "overlay_D1286C43_C5DC_AEE6_41C0_DE150E113840",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "toolTipPaddingTop": 4,
 "progressBorderRadius": 0,
 "id": "ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9C",
 "left": "0%",
 "width": "100%",
 "paddingBottom": 0,
 "toolTipPaddingRight": 6,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "toolTipDisplayTime": 600,
 "toolTipPaddingLeft": 6,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "playbackBarHeadHeight": 15,
 "minHeight": 1,
 "playbackBarBottom": 0,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBorderColor": "#FFFFFF",
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 1,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 1,
 "height": "100%",
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": 12,
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "show": "this.ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9C.bind('hide', function(e){ e.source.unbind('hide', arguments.callee, this); this.playList_D327C3D5_C6F4_F36F_41E1_A4B0F1A5D753.set('selectedIndex', -1); }, this); this.playList_D327C3D5_C6F4_F36F_41E1_A4B0F1A5D753.set('selectedIndex', 0)",
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "toolTipShadowHorizontalLength": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": false,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "top": "0%",
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 2000,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "toolTipShadowVerticalLength": 0,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 2,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "Viewer photoalbum 1"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866, this.camera_D055F49E_C6F4_F51D_41D9_CA8C490D0D63); this.mainPlayList.set('selectedIndex', 10)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -91.39,
   "hfov": 16.58,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_1_HS_0_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -22.38
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B60B64_C5F5_AAA2_41DC_3C8041ED4D9C",
   "yaw": -91.39,
   "pitch": -22.38,
   "distance": 100,
   "hfov": 16.58
  }
 ],
 "id": "overlay_D99058AF_C5F4_B7BE_4187_9AC8F1148FDC",
 "data": {
  "label": "Circle Arrow 04b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6, this.camera_D065C494_C6F4_F5ED_41E5_58E746A3DD5B); this.mainPlayList.set('selectedIndex', 11)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 88.35,
   "hfov": 16.54,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_1_HS_1_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -22.73
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B69B64_C5F5_AAA2_41CC_6379C744B359",
   "yaw": 88.35,
   "pitch": -22.73,
   "distance": 100,
   "hfov": 16.54
  }
 ],
 "id": "overlay_D89A713C_C5F7_B6A3_41DB_D9099C3CB357",
 "data": {
  "label": "Circle Arrow 04b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866, this.camera_D0CD8432_C6F4_F525_419E_FA03BDC5210D); this.mainPlayList.set('selectedIndex', 10)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 3.23,
   "hfov": 10.48,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_1_HS_0_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -3.79
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0AC7B61_C5F5_AAA2_41CE_A507FCC3E25E",
   "yaw": 3.23,
   "pitch": -3.79,
   "distance": 100,
   "hfov": 10.48
  }
 ],
 "id": "overlay_D70485AC_C5DC_B9A2_41E4_F754FBFB89FC",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2, this.camera_D0D83421_C6F4_F527_41BE_22DC486ABD33); this.mainPlayList.set('selectedIndex', 0)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 56.01,
   "hfov": 9.61,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_1_HS_1_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -10.2
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0ACAB61_C5F5_AAA2_41D3_BCB2D5C2ADB6",
   "yaw": 56.01,
   "pitch": -10.2,
   "distance": 100,
   "hfov": 9.61
  }
 ],
 "id": "overlay_D6931C52_C5DF_EEE7_41CC_942AF1E9953A",
 "data": {
  "label": "Circle Arrow 02b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC, this.camera_D1B3950C_C6F4_F4FD_41B4_DAC80AD6C46E); this.mainPlayList.set('selectedIndex', 3)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 90.55,
   "hfov": 8.8,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_0_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -2.61
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B0CB63_C5F5_AAA6_41DB_A7223D2228E4",
   "yaw": 90.55,
   "pitch": -2.61,
   "distance": 100,
   "hfov": 8.8
  }
 ],
 "id": "overlay_D9C33A3D_C5F3_AA9D_41BF_DA2CBE43A675",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299, this.camera_D1A38517_C6F4_F4EB_41E5_442F87F2802F); this.mainPlayList.set('selectedIndex', 12)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -32.23,
   "hfov": 8.81,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_1_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -2.26
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B71B63_C5F5_AAA6_41D6_D752335C9957",
   "yaw": -32.23,
   "pitch": -2.26,
   "distance": 100,
   "hfov": 8.81
  }
 ],
 "id": "overlay_D962094E_C5F3_D6FE_41E2_E5CBD0D4A80D",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5, this.camera_D1924521_C6F4_F727_41DC_F175F1EBFE1C); this.mainPlayList.set('selectedIndex', 1)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -113.33,
   "hfov": 8.8,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_2_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -3.3
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B7AB64_C5F5_AAA2_4189_BB6DD135D286",
   "yaw": -113.33,
   "pitch": -3.3,
   "distance": 100,
   "hfov": 8.8
  }
 ],
 "id": "overlay_D9156281_C5F3_5A62_41E5_A65E9FCB0F66",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF54088_C5CF_5662_41DA_C2352BB20299, this.camera_D427F570_C6F4_F725_41D6_1B0CD9F28B53); this.mainPlayList.set('selectedIndex', 12)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -99.37,
   "hfov": 16.96,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_1_HS_0_0_0_map.gif",
      "width": 36,
      "height": 16
     }
    ]
   },
   "pitch": -18.91
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B7EB64_C5F5_AAA2_41D4_8577238D5DEC",
   "yaw": -99.37,
   "pitch": -18.91,
   "distance": 100,
   "hfov": 16.96
  }
 ],
 "id": "overlay_D89E003E_C5F4_B69E_41DC_FD918FB32892",
 "data": {
  "label": "Circle Arrow 04b"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC, this.camera_D0A22460_C6F4_F526_41E8_17BC28C48D2B); this.mainPlayList.set('selectedIndex', 3)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": 59.6,
   "hfov": 12.44,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_0_0_0_map.gif",
      "width": 61,
      "height": 16
     }
    ]
   },
   "pitch": -13.68
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B14B62_C5F5_AAA6_41D6_7C48E4CC069F",
   "yaw": 59.6,
   "pitch": -13.68,
   "distance": 100,
   "hfov": 12.44
  }
 ],
 "id": "overlay_D26CFFBF_C5D4_A99E_41DC_44D974D41F81",
 "data": {
  "label": "Circle Arrow 03c"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59, this.camera_D091A470_C6F4_F525_4195_68A4B96F0EA4); this.mainPlayList.set('selectedIndex', 4)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -12.62,
   "hfov": 9.28,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_1_0_0_map.gif",
      "width": 61,
      "height": 16
     }
    ]
   },
   "pitch": -10.99
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B1EB63_C5F5_AAA6_41DF_FBEC47ADAD8C",
   "yaw": -12.62,
   "pitch": -10.99,
   "distance": 100,
   "hfov": 9.28
  }
 ],
 "id": "overlay_DCB5EA2F_C5D3_6ABE_41B5_42913216DFC3",
 "data": {
  "label": "Circle Arrow 03c"
 },
 "rollOverDisplay": false
},
{
 "useHandCursor": true,
 "areas": [
  {
   "class": "HotspotPanoramaOverlayArea",
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0, this.camera_D087947F_C6F4_F51B_41E0_D5B614464B01); this.mainPlayList.set('selectedIndex', 6)"
  }
 ],
 "maps": [
  {
   "class": "HotspotPanoramaOverlayMap",
   "yaw": -122.43,
   "hfov": 9.18,
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "class": "ImageResourceLevel",
      "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_2_0_0_map.gif",
      "width": 16,
      "height": 16
     }
    ]
   },
   "pitch": -0.79
  }
 ],
 "enabledInCardboard": true,
 "class": "HotspotPanoramaOverlay",
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_E0B02B63_C5F5_AAA6_41E6_7D2BCD9FEFAD",
   "yaw": -122.43,
   "pitch": -0.79,
   "distance": 100,
   "hfov": 9.18
  }
 ],
 "id": "overlay_DD1367E9_C5D3_79A2_41DB_116CCE6C83FF",
 "data": {
  "label": "Circle Door 01"
 },
 "rollOverDisplay": false
},
{
 "children": [
  "this.IconButton_EF8F8BD8_E386_8E02_41D6_310FF1964329"
 ],
 "backgroundOpacity": 0,
 "id": "Container_EF8F8BD8_E386_8E02_41E5_FC5C5513733A",
 "width": 110,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingBottom": 0,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "verticalAlign": "middle",
 "overflow": "visible",
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "height": 110,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "center",
 "gap": 10,
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "button menu sup"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "children": [
  "this.IconButton_EF7806FA_E38F_8606_41E5_5C4557EBCACB",
  "this.IconButton_EE9FBAB2_E389_8E06_41D7_903ABEDD153A",
  "this.IconButton_EED073D3_E38A_9E06_41E1_6CCC9722545D",
  "this.IconButton_EEEB3760_E38B_8603_41D6_FE6B11A3DA96",
  "this.IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0",
  "this.IconButton_EE5807F6_E3BE_860E_41E7_431DDDA54BAC",
  "this.IconButton_EED5213F_E3B9_7A7D_41D8_1B642C004521"
 ],
 "backgroundOpacity": 0,
 "id": "Container_EF8F8BD8_E386_8E02_41E5_90850B5F0BBE",
 "width": "91.304%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "0%",
 "paddingBottom": 0,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "verticalAlign": "top",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "center",
 "gap": 3,
 "height": "85.959%",
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "-button set"
 },
 "shadow": false,
 "layout": "vertical",
 "visible": false
},
{
 "fontFamily": "Bebas Neue Bold",
 "textShadowHorizontalLength": 0,
 "backgroundOpacity": 0,
 "id": "Label_0DD14F09_1744_0507_41AA_D8475423214A",
 "left": 0,
 "width": 180,
 "borderRadius": 0,
 "paddingBottom": 0,
 "textShadowVerticalLength": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "textShadowBlurRadius": 10,
 "minHeight": 1,
 "text": "CASA A",
 "top": 5,
 "minWidth": 1,
 "height": 86,
 "fontSize": "54px",
 "class": "Label",
 "horizontalAlign": "left",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "textShadowOpacity": 1,
 "paddingTop": 0,
 "data": {
  "name": "text 1"
 },
 "shadow": false,
 "fontWeight": "bold",
 "textDecoration": "none",
 "textShadowColor": "#000000"
},
{
 "fontFamily": "Bebas Neue Book",
 "textShadowHorizontalLength": 0,
 "backgroundOpacity": 0,
 "id": "Label_0DD1AF09_1744_0507_41B4_9F5A60B503B2",
 "left": 0,
 "width": 316,
 "borderRadius": 0,
 "paddingBottom": 0,
 "textShadowVerticalLength": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "textShadowBlurRadius": 10,
 "minHeight": 1,
 "text": "submission by ahmdaini",
 "bottom": 27,
 "minWidth": 1,
 "height": 46,
 "fontSize": "24px",
 "class": "Label",
 "horizontalAlign": "left",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "textShadowOpacity": 1,
 "paddingTop": 0,
 "data": {
  "name": "text 2"
 },
 "shadow": false,
 "fontWeight": "normal",
 "textDecoration": "none",
 "textShadowColor": "#000000"
},
{
 "maxHeight": 2,
 "id": "Image_1B99DD00_16C4_0505_41B3_51F09727447A",
 "left": "0%",
 "backgroundOpacity": 0,
 "maxWidth": 3000,
 "borderRadius": 0,
 "right": "0%",
 "paddingBottom": 0,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "url": "skin/Image_1B99DD00_16C4_0505_41B3_51F09727447A.png",
 "minHeight": 1,
 "verticalAlign": "middle",
 "bottom": 53,
 "minWidth": 1,
 "height": 2,
 "class": "Image",
 "horizontalAlign": "center",
 "paddingTop": 0,
 "scaleMode": "fit_outside",
 "data": {
  "name": "white line"
 },
 "shadow": false
},
{
 "children": [
  "this.Button_1B999D00_16C4_0505_41AB_D0C2E7857448",
  "this.Button_1B9A4D00_16C4_0505_4193_E0EA69B0CBB0",
  "this.Button_1B9A5D00_16C4_0505_41B0_D18F25F377C4"
 ],
 "backgroundOpacity": 0,
 "id": "Container_1B99BD00_16C4_0505_41A4_A3C2452B0288",
 "left": "0%",
 "width": 420,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 30,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "overflow": "scroll",
 "bottom": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "height": 51,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 3,
 "scrollBarOpacity": 0.5,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "-button set container"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "children": [
  "this.Container_062A682F_1140_E20B_41B0_3071FCBF3DC9",
  "this.Container_062A082F_1140_E20A_4193_DF1A4391DC79"
 ],
 "backgroundOpacity": 1,
 "id": "Container_062A782F_1140_E20B_41AF_B3E5DE341773",
 "left": "10%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "10%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "top": "5%",
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "left",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "horizontal",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.IconButton_062A8830_1140_E215_419D_3439F16CCB3E"
 ],
 "backgroundOpacity": 0,
 "id": "Container_062A9830_1140_E215_41A7_5F2BBE5C20E4",
 "left": "10%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "10%",
 "paddingBottom": 0,
 "paddingRight": 20,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "verticalAlign": "top",
 "overflow": "visible",
 "top": "5%",
 "bottom": "80%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 10,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container X global"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "children": [
  "this.Container_23F797B7_0C0A_6293_41A7_EC89DBCDB93F",
  "this.Container_23F027B7_0C0A_6293_418E_075FCFAA8A19"
 ],
 "backgroundOpacity": 1,
 "id": "Container_23F7B7B7_0C0A_6293_4197_F931EEC6FA48",
 "left": "10%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "10%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "top": "5%",
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "left",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "horizontal",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.IconButton_23F087B8_0C0A_629D_4194_6F34C6CBE1DA"
 ],
 "backgroundOpacity": 0,
 "id": "Container_23F097B8_0C0A_629D_4176_D87C90BA32B6",
 "left": "10%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "10%",
 "paddingBottom": 0,
 "paddingRight": 20,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "verticalAlign": "top",
 "overflow": "visible",
 "top": "5%",
 "bottom": "80%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 10,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container X global"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "children": [
  "this.Container_3A67552A_0C3A_67BD_4195_ECE46CCB34EA",
  "this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0"
 ],
 "backgroundOpacity": 1,
 "id": "Container_39A197B1_0C06_62AF_419A_D15E4DDD2528",
 "left": "2%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "2%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "visible",
 "top": "5%",
 "backgroundColorRatios": [
  0,
  1
 ],
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "center",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "vertical",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.Container_2F8A7686_0D4F_6B71_41A9_1A894413085C",
  "this.MapViewer"
 ],
 "backgroundOpacity": 1,
 "id": "Container_2F8A6686_0D4F_6B71_4174_A02FE43588D3",
 "left": "2%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "2%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "visible",
 "top": "5%",
 "backgroundColorRatios": [
  0,
  1
 ],
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "center",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "vertical",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.Container_28214A13_0D5D_5B97_4193_B631E1496339",
  "this.Container_2B0BF61C_0D5B_2B90_4179_632488B1209E"
 ],
 "backgroundOpacity": 1,
 "id": "Container_28215A13_0D5D_5B97_4198_A7CA735E9E0A",
 "left": "15%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "15%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "visible",
 "top": "7%",
 "bottom": "7%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "center",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "vertical",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.Container_2A19EC4C_0D3B_DFF0_414D_37145C22C5BC"
 ],
 "backgroundOpacity": 1,
 "id": "Container_2A193C4C_0D3B_DFF0_4161_A2CD128EF536",
 "left": "2%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "2%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "visible",
 "top": "5%",
 "backgroundColorRatios": [
  0,
  1
 ],
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "center",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "vertical",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.Container_06C5ABA5_1140_A63F_41A9_850CF958D0DB",
  "this.Container_06C58BA5_1140_A63F_419D_EC83F94F8C54"
 ],
 "backgroundOpacity": 1,
 "id": "Container_06C5DBA5_1140_A63F_41AD_1D83A33F1255",
 "left": "10%",
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "right": "10%",
 "paddingRight": 0,
 "shadowOpacity": 0.3,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "top": "5%",
 "bottom": "5%",
 "verticalAlign": "top",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "shadowHorizontalLength": 0,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "scrollBarWidth": 10,
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "left",
 "scrollBarOpacity": 0.5,
 "shadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Global"
 },
 "shadow": true,
 "layout": "horizontal",
 "shadowBlurRadius": 25,
 "shadowSpread": 1
},
{
 "children": [
  "this.IconButton_06C40BA5_1140_A63F_41AC_FA560325FD81"
 ],
 "backgroundOpacity": 0,
 "id": "Container_06C43BA5_1140_A63F_41A1_96DC8F4CAD2F",
 "left": "10%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "right": "10%",
 "paddingBottom": 0,
 "paddingRight": 20,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "verticalAlign": "top",
 "overflow": "visible",
 "top": "5%",
 "bottom": "80%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 10,
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container X global"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_D31F43D8_C6F4_F365_41E3_8A043F0450C3",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0_HS_0_0.png",
   "width": 1220,
   "height": 480
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B2EB62_C5F5_AAA6_41E7_741F7D4BF8C5",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_1_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_D31FB3D8_C6F4_F365_41D5_5E35DA96E2B3",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF5612A_C5CF_D6A6_418A_39B77E1CBC59_0_HS_2_0.png",
   "width": 1220,
   "height": 480
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F5512601_C5D4_DA62_41E6_83BC492D6B25",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF8B097_C5CF_B66E_41E0_566B8D4D640D_0_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "class": "PhotoAlbumPlayer",
 "viewerArea": "this.viewer_uidD32AC3CC_C6F4_F37D_41BB_1BA4830B6B48",
 "id": "viewer_uidD32AC3CC_C6F4_F37D_41BB_1BA4830B6B48PhotoAlbumPlayer",
 "buttonPrevious": [
  "this.IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD",
  "this.IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D",
  "this.IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482"
 ],
 "buttonNext": [
  "this.IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4",
  "this.IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14",
  "this.IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510"
 ]
},
{
 "progressBorderRadius": 0,
 "toolTipPaddingRight": 6,
 "id": "viewer_uidD32AC3CC_C6F4_F37D_41BB_1BA4830B6B48",
 "width": "100%",
 "paddingBottom": 0,
 "toolTipPaddingTop": 4,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "toolTipDisplayTime": 600,
 "toolTipPaddingLeft": 6,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "playbackBarHeadHeight": 15,
 "minHeight": 50,
 "playbackBarBottom": 0,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBorderColor": "#FFFFFF",
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 100,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 1,
 "height": "100%",
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": "1.11vmin",
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "toolTipShadowHorizontalLength": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": false,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 2000,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "toolTipShadowVerticalLength": 0,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 2,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "ViewerArea7091"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "showEffect": {
  "class": "FadeInEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "backgroundOpacity": 0.7,
 "id": "htmltext_D32443CE_C6F4_F37D_41AF_4D11F0A48E7D",
 "width": "100%",
 "paddingBottom": 5,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 10,
 "paddingLeft": 10,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "backgroundColorRatios": [
  0
 ],
 "backgroundColor": [
  "#000000"
 ],
 "hideEffect": {
  "class": "FadeOutEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "minWidth": 0,
 "scrollBarColor": "#000000",
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "html": "",
 "scrollBarMargin": 2,
 "paddingTop": 5,
 "data": {
  "name": "HTMLText7094"
 },
 "shadow": false,
 "visible": false
},
{
 "backgroundOpacity": 0,
 "id": "component_D32623D4_C6F4_F36D_41CC_17C0EE8A3F46",
 "left": 10,
 "paddingBottom": 0,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "top": "45%",
 "hideEffect": {
  "class": "FadeOutEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "minWidth": 0,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "click": "this.loadFromCurrentMediaPlayList(this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_AlbumPlayList, -1)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton7095"
 },
 "shadow": false,
 "visible": false,
 "showEffect": {
  "class": "FadeInEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "iconURL": "skin/album_left.png",
 "cursor": "hand"
},
{
 "backgroundOpacity": 0,
 "id": "component_D32603D4_C6F4_F36D_41CD_26243E079EA8",
 "paddingBottom": 0,
 "borderRadius": 0,
 "right": 10,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "verticalAlign": "middle",
 "top": "45%",
 "hideEffect": {
  "class": "FadeOutEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "minWidth": 0,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "click": "this.loadFromCurrentMediaPlayList(this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5_AlbumPlayList, 1)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton7096"
 },
 "shadow": false,
 "visible": false,
 "showEffect": {
  "class": "FadeInEffect",
  "duration": 250,
  "easing": "cubic_in_out"
 },
 "iconURL": "skin/album_right.png",
 "cursor": "hand"
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F56C65FA_C5D4_D9A6_41D1_7ABDB7E2802C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0_HS_0_0.png",
   "width": 1080,
   "height": 720
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F56DA5FA_C5D4_D9A6_41C6_32B4DA3E097C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CE3D003C_C5CF_F6A2_41B1_F17FC41FD7D2_0_HS_1_0.png",
   "width": 1080,
   "height": 720
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F50A3206_C5DD_5A6E_41BB_CB6B1A590729",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_0_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F50A7206_C5DD_5A6E_41C0_F87D2AB805E1",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F50AD206_C5DD_5A6E_41E2_4F2DFB872F5C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_2_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F5092207_C5DD_5A6E_41D1_924F0D278D98",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF84CEB_C5CF_EFA5_41DA_87946C15A2AC_0_HS_3_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B71B63_C5F5_AAA6_41E2_AEF5F1C3393C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_1_HS_0_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B74B63_C5F5_AAA6_41D0_EE1782E25501",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF8DCB5_C5CF_AFAD_41BA_C832D5796180_1_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B04B63_C5F5_AAA6_41D3_35A9ABC49322",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_1_HS_0_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B0FB63_C5F5_AAA6_41DB_0D69E2C8C80E",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF678E0_C5CF_D7A2_41A3_BDC73378F3D0_1_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_F556C601_C5D4_DA62_41D2_745B0780B408",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB547A_C5CF_BEA6_41E6_2F2462CC24B9_0_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0ACDB61_C5F5_AAA2_41C7_82B03F58FDBA",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_1_HS_0_0.png",
   "width": 1080,
   "height": 720
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B30B61_C5F5_AAA2_41DA_A67FDA4E68E6",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF8C92B_C5CF_F6A6_41E5_D7F9A793590E_1_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B60B64_C5F5_AAA2_41DC_3C8041ED4D9C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_1_HS_0_0.png",
   "width": 1200,
   "height": 780
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B69B64_C5F5_AAA2_41CC_6379C744B359",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF54088_C5CF_5662_41DA_C2352BB20299_1_HS_1_0.png",
   "width": 1200,
   "height": 780
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0AC7B61_C5F5_AAA2_41CE_A507FCC3E25E",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_1_HS_0_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0ACAB61_C5F5_AAA2_41D3_BCB2D5C2ADB6",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEE4656E_C5CF_FEBF_41DE_34AE0272DDA5_1_HS_1_0.png",
   "width": 1080,
   "height": 720
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B0CB63_C5F5_AAA6_41DB_A7223D2228E4",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_0_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B71B63_C5F5_AAA6_41D6_D752335C9957",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_1_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B7AB64_C5F5_AAA2_4189_BB6DD135D286",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB086C_C5CF_B6A3_41B8_E4789547F866_1_HS_2_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B7EB64_C5F5_AAA2_41D4_8577238D5DEC",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEF42CB5_C5CF_AFA2_41E7_F0C2BA48B1D6_1_HS_0_0.png",
   "width": 1200,
   "height": 780
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B14B62_C5F5_AAA6_41D6_7C48E4CC069F",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_0_0.png",
   "width": 1220,
   "height": 480
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B1EB63_C5F5_AAA6_41DF_FBEC47ADAD8C",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_1_0.png",
   "width": 1220,
   "height": 480
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_E0B02B63_C5F5_AAA6_41E6_7D2BCD9FEFAD",
 "colCount": 4,
 "class": "AnimatedImageResource",
 "frameCount": 24,
 "levels": [
  {
   "class": "ImageResourceLevel",
   "url": "media/panorama_CEFB84FE_C5CF_DF9E_4186_F33F0675FD30_1_HS_2_0.png",
   "width": 800,
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "maxHeight": 60,
 "backgroundOpacity": 0,
 "id": "IconButton_EF8F8BD8_E386_8E02_41D6_310FF1964329",
 "width": 60,
 "paddingBottom": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 60,
 "minWidth": 1,
 "mode": "toggle",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_EF8F8BD8_E386_8E02_41D6_310FF1964329_pressed.png",
 "click": "if(!this.Container_EF8F8BD8_E386_8E02_41E5_90850B5F0BBE.get('visible')){ this.setComponentVisibility(this.Container_EF8F8BD8_E386_8E02_41E5_90850B5F0BBE, true, 0, null, null, false) } else { this.setComponentVisibility(this.Container_EF8F8BD8_E386_8E02_41E5_90850B5F0BBE, false, 0, null, null, false) }",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "image button menu"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EF8F8BD8_E386_8E02_41D6_310FF1964329.png",
 "cursor": "hand"
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EE5807F6_E3BE_860E_41E7_431DDDA54BAC",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "rollOverIconURL": "skin/IconButton_EE5807F6_E3BE_860E_41E7_431DDDA54BAC_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "click": "this.shareTwitter(window.location.href)",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton TWITTER"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EE5807F6_E3BE_860E_41E7_431DDDA54BAC.png",
 "cursor": "hand"
},
{
 "maxHeight": 58,
 "backgroundOpacity": 0,
 "id": "IconButton_EED5213F_E3B9_7A7D_41D8_1B642C004521",
 "width": 58,
 "paddingBottom": 0,
 "maxWidth": 58,
 "borderRadius": 0,
 "rollOverIconURL": "skin/IconButton_EED5213F_E3B9_7A7D_41D8_1B642C004521_rollover.png",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "height": 58,
 "minWidth": 1,
 "mode": "push",
 "class": "IconButton",
 "horizontalAlign": "center",
 "click": "this.shareFacebook(window.location.href)",
 "transparencyActive": true,
 "paddingTop": 0,
 "data": {
  "name": "IconButton FB"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_EED5213F_E3B9_7A7D_41D8_1B642C004521.png",
 "cursor": "hand"
},
{
 "textDecoration": "none",
 "fontFamily": "Montserrat",
 "rollOverBackgroundOpacity": 0.8,
 "backgroundOpacity": 0,
 "id": "Button_1B999D00_16C4_0505_41AB_D0C2E7857448",
 "width": 130,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "iconHeight": 32,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "iconWidth": 32,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "pressedBackgroundColorRatios": [
  0
 ],
 "rollOverBackgroundColor": [
  "#04A3E1"
 ],
 "pressedBackgroundOpacity": 1,
 "borderColor": "#000000",
 "rollOverBackgroundColorRatios": [
  0
 ],
 "pressedBackgroundColor": [
  "#000000"
 ],
 "iconBeforeLabel": true,
 "minWidth": 1,
 "mode": "push",
 "height": 40,
 "fontSize": "10px",
 "class": "Button",
 "horizontalAlign": "center",
 "gap": 5,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "backgroundColorDirection": "vertical",
 "label": "PANORAMA LIST",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "layout": "horizontal",
 "paddingTop": 0,
 "data": {
  "name": "Button panorama list"
 },
 "shadow": false,
 "fontWeight": "bold",
 "shadowBlurRadius": 15,
 "shadowSpread": 1,
 "cursor": "hand",
 "click": "this.setComponentVisibility(this.Container_39DE87B1_0C06_62AF_417B_8CB0FB5C9D15, true, 0, null, null, false)"
},
{
 "textDecoration": "none",
 "fontFamily": "Montserrat",
 "rollOverBackgroundOpacity": 0.8,
 "backgroundOpacity": 0,
 "id": "Button_1B9A4D00_16C4_0505_4193_E0EA69B0CBB0",
 "width": 103,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "iconHeight": 32,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "iconWidth": 32,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "pressedBackgroundColorRatios": [
  0
 ],
 "rollOverBackgroundColor": [
  "#04A3E1"
 ],
 "pressedBackgroundOpacity": 1,
 "borderColor": "#000000",
 "rollOverBackgroundColorRatios": [
  0
 ],
 "pressedBackgroundColor": [
  "#000000"
 ],
 "iconBeforeLabel": true,
 "minWidth": 1,
 "mode": "push",
 "height": 40,
 "fontSize": "10px",
 "class": "Button",
 "horizontalAlign": "center",
 "gap": 5,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "backgroundColorDirection": "vertical",
 "label": "LAYOUT PLAN",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "layout": "horizontal",
 "paddingTop": 0,
 "data": {
  "name": "Button floorplan"
 },
 "shadow": false,
 "fontWeight": "bold",
 "shadowBlurRadius": 15,
 "shadowSpread": 1,
 "cursor": "hand",
 "click": "this.setComponentVisibility(this.Container_2F8BB687_0D4F_6B7F_4190_9490D02FBC41, true, 0, null, null, false)"
},
{
 "textDecoration": "none",
 "fontFamily": "Montserrat",
 "rollOverBackgroundOpacity": 0.8,
 "backgroundOpacity": 0,
 "id": "Button_1B9A5D00_16C4_0505_41B0_D18F25F377C4",
 "width": 112,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "iconHeight": 32,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "iconWidth": 32,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": true,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "pressedBackgroundColorRatios": [
  0
 ],
 "rollOverBackgroundColor": [
  "#04A3E1"
 ],
 "pressedBackgroundOpacity": 1,
 "borderColor": "#000000",
 "rollOverBackgroundColorRatios": [
  0
 ],
 "pressedBackgroundColor": [
  "#000000"
 ],
 "iconBeforeLabel": true,
 "minWidth": 1,
 "mode": "push",
 "height": 40,
 "fontSize": "10px",
 "class": "Button",
 "horizontalAlign": "center",
 "gap": 5,
 "backgroundColor": [
  "#000000",
  "#000000"
 ],
 "backgroundColorDirection": "vertical",
 "label": "PHOTOALBUM",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "layout": "horizontal",
 "paddingTop": 0,
 "data": {
  "name": "Button photoalbum"
 },
 "shadow": false,
 "fontWeight": "bold",
 "shadowBlurRadius": 15,
 "shadowSpread": 1,
 "cursor": "hand",
 "click": "this.setComponentVisibility(this.Container_2A1A5C4D_0D3B_DFF0_41A9_8FC811D03C8E, true, 0, null, null, false)"
},
{
 "children": [
  "this.Image_062A182F_1140_E20B_41B0_9CB8FFD6AA5A"
 ],
 "backgroundOpacity": 1,
 "id": "Container_062A682F_1140_E20B_41B0_3071FCBF3DC9",
 "width": "85%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "center",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "-left"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Container_062A3830_1140_E215_4195_1698933FE51C",
  "this.Container_062A2830_1140_E215_41AA_EB25B7BD381C",
  "this.Container_062AE830_1140_E215_4180_196ED689F4BD"
 ],
 "backgroundOpacity": 1,
 "id": "Container_062A082F_1140_E20A_4193_DF1A4391DC79",
 "width": "50%",
 "paddingBottom": 20,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 50,
 "verticalAlign": "top",
 "paddingLeft": 50,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#0069A3",
 "minWidth": 460,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.51,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "-right"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "maxHeight": 60,
 "id": "IconButton_062A8830_1140_E215_419D_3439F16CCB3E",
 "width": "25%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_062A8830_1140_E215_419D_3439F16CCB3E_rollover.jpg",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "minWidth": 50,
 "mode": "push",
 "height": "75%",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_062A8830_1140_E215_419D_3439F16CCB3E_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_062AB830_1140_E215_41AF_6C9D65345420, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_062A8830_1140_E215_419D_3439F16CCB3E.jpg",
 "cursor": "hand"
},
{
 "children": [
  "this.ViewerAreaLabeled_23F787B7_0C0A_6293_419A_B4B58B92DAFC",
  "this.Container_23F7F7B7_0C0A_6293_4195_D6240EBAFDC0"
 ],
 "backgroundOpacity": 1,
 "id": "Container_23F797B7_0C0A_6293_41A7_EC89DBCDB93F",
 "width": "85%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "center",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "-left"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Container_23F017B8_0C0A_629D_41A5_DE420F5F9331",
  "this.Container_23F007B8_0C0A_629D_41A3_034CF0D91203",
  "this.Container_23F047B8_0C0A_629D_415D_F05EF8619564"
 ],
 "backgroundOpacity": 1,
 "id": "Container_23F027B7_0C0A_6293_418E_075FCFAA8A19",
 "width": "50%",
 "paddingBottom": 20,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 50,
 "verticalAlign": "top",
 "paddingLeft": 50,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#0069A3",
 "minWidth": 460,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.51,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "-right"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "maxHeight": 60,
 "id": "IconButton_23F087B8_0C0A_629D_4194_6F34C6CBE1DA",
 "width": "25%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_23F087B8_0C0A_629D_4194_6F34C6CBE1DA_rollover.jpg",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "minWidth": 50,
 "mode": "push",
 "height": "75%",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_23F087B8_0C0A_629D_4194_6F34C6CBE1DA_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_23F0F7B8_0C0A_629D_418A_F171085EFBF8, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_23F087B8_0C0A_629D_4194_6F34C6CBE1DA.jpg",
 "cursor": "hand"
},
{
 "children": [
  "this.HTMLText_3918BF37_0C06_E393_41A1_17CF0ADBAB12",
  "this.IconButton_38922473_0C06_2593_4199_C585853A1AB3"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_3A67552A_0C3A_67BD_4195_ECE46CCB34EA",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 140,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "header"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "itemVerticalAlign": "top",
 "selectedItemLabelFontWeight": "bold",
 "id": "ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0",
 "itemLabelFontColor": "#666666",
 "playList": "this.ThumbnailList_034EDD7A_0D3B_3991_41A5_D706671923C0_playlist",
 "paddingBottom": 70,
 "scrollBarWidth": 10,
 "borderRadius": 5,
 "width": "100%",
 "rollOverItemThumbnailShadowBlurRadius": 0,
 "itemBackgroundColorDirection": "vertical",
 "itemLabelGap": 7,
 "verticalAlign": "middle",
 "paddingLeft": 70,
 "borderSize": 0,
 "rollOverItemThumbnailShadowVerticalLength": 0,
 "minHeight": 1,
 "itemPaddingBottom": 3,
 "backgroundColorRatios": [
  0
 ],
 "scrollBarColor": "#04A3E1",
 "itemThumbnailHeight": 125,
 "minWidth": 1,
 "itemLabelFontStyle": "normal",
 "selectedItemLabelFontColor": "#04A3E1",
 "backgroundColor": [
  "#000000"
 ],
 "itemOpacity": 1,
 "scrollBarVisible": "rollOver",
 "class": "ThumbnailGrid",
 "selectedItemThumbnailShadowHorizontalLength": 0,
 "itemMode": "normal",
 "gap": 26,
 "scrollBarOpacity": 0.5,
 "itemLabelHorizontalAlign": "center",
 "rollOverItemThumbnailShadowColor": "#04A3E1",
 "itemMaxWidth": 1000,
 "itemLabelFontFamily": "Montserrat",
 "itemThumbnailWidth": 220,
 "itemMaxHeight": 1000,
 "paddingTop": 10,
 "shadow": false,
 "selectedItemThumbnailShadowBlurRadius": 16,
 "itemHorizontalAlign": "center",
 "itemBorderRadius": 0,
 "itemPaddingLeft": 3,
 "itemLabelPosition": "bottom",
 "itemThumbnailShadow": false,
 "backgroundOpacity": 0.05,
 "height": "100%",
 "selectedItemThumbnailShadow": true,
 "rollOverItemLabelFontColor": "#04A3E1",
 "itemThumbnailBorderRadius": 0,
 "itemBackgroundOpacity": 0,
 "paddingRight": 70,
 "itemPaddingTop": 3,
 "propagateClick": false,
 "itemWidth": 220,
 "itemBackgroundColor": [],
 "itemBackgroundColorRatios": [],
 "itemThumbnailOpacity": 1,
 "horizontalAlign": "center",
 "rollOverItemThumbnailShadowHorizontalLength": 8,
 "itemMinHeight": 50,
 "backgroundColorDirection": "vertical",
 "itemPaddingRight": 3,
 "itemLabelTextDecoration": "none",
 "itemLabelFontWeight": "normal",
 "selectedItemThumbnailShadowVerticalLength": 0,
 "scrollBarMargin": 2,
 "data": {
  "name": "ThumbnailList"
 },
 "rollOverItemThumbnailShadow": true,
 "itemHeight": 156,
 "itemThumbnailScaleMode": "fit_outside",
 "itemLabelFontSize": 14,
 "itemMinWidth": 50
},
{
 "children": [
  "this.HTMLText_2F8A4686_0D4F_6B71_4183_10C1696E2923",
  "this.IconButton_2F8A5686_0D4F_6B71_41A1_13CF877A165E"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_2F8A7686_0D4F_6B71_41A9_1A894413085C",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 140,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "header"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.HTMLText_28217A13_0D5D_5B97_419A_F894ECABEB04",
  "this.IconButton_28216A13_0D5D_5B97_41A9_2CAB10DB6CA3"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_28214A13_0D5D_5B97_4193_B631E1496339",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 140,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "header"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.ViewerAreaLabeled_281D2361_0D5F_E9B0_41A1_A1F237F85FD7",
  "this.IconButton_2BE71718_0D55_6990_41A5_73D31D902E1D",
  "this.IconButton_28BF3E40_0D4B_DBF0_41A3_D5D2941E6E14"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_2B0BF61C_0D5B_2B90_4179_632488B1209E",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container photo"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.ViewerAreaLabeled_2A198C4C_0D3B_DFF0_419F_C9A785406D9C",
  "this.IconButton_2A19BC4C_0D3B_DFF0_419F_D0DCB12FF482",
  "this.IconButton_2A19AC4C_0D3B_DFF0_4181_A2C230C2E510",
  "this.IconButton_2A19CC4C_0D3B_DFF0_41AA_D2AC34177CF1"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_2A19EC4C_0D3B_DFF0_414D_37145C22C5BC",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container photo"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Image_06C5BBA5_1140_A63F_41A7_E6D01D4CC397"
 ],
 "backgroundOpacity": 1,
 "id": "Container_06C5ABA5_1140_A63F_41A9_850CF958D0DB",
 "width": "55%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#000000"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "center",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "-left"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "children": [
  "this.Container_06C59BA5_1140_A63F_41B1_4B41E3B7D98D",
  "this.Container_06C46BA5_1140_A63F_4151_B5A20B4EA86A",
  "this.Container_06C42BA5_1140_A63F_4195_037A0687532F"
 ],
 "backgroundOpacity": 1,
 "id": "Container_06C58BA5_1140_A63F_419D_EC83F94F8C54",
 "width": "45%",
 "paddingBottom": 20,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 60,
 "verticalAlign": "top",
 "paddingLeft": 60,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#0069A3",
 "minWidth": 460,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.51,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "-right"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "maxHeight": 60,
 "id": "IconButton_06C40BA5_1140_A63F_41AC_FA560325FD81",
 "width": "25%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_06C40BA5_1140_A63F_41AC_FA560325FD81_rollover.jpg",
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "minWidth": 50,
 "mode": "push",
 "height": "75%",
 "class": "IconButton",
 "horizontalAlign": "center",
 "pressedIconURL": "skin/IconButton_06C40BA5_1140_A63F_41AC_FA560325FD81_pressed.jpg",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_06C40BA5_1140_A63F_41AC_FA560325FD81.jpg",
 "cursor": "hand"
},
{
 "maxHeight": 1000,
 "id": "Image_062A182F_1140_E20B_41B0_9CB8FFD6AA5A",
 "left": "0%",
 "width": "100%",
 "backgroundOpacity": 0,
 "borderRadius": 0,
 "paddingBottom": 0,
 "maxWidth": 2000,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "url": "skin/Image_062A182F_1140_E20B_41B0_9CB8FFD6AA5A.jpg",
 "minHeight": 1,
 "top": "0%",
 "minWidth": 1,
 "height": "100%",
 "class": "Image",
 "horizontalAlign": "center",
 "paddingTop": 0,
 "scaleMode": "fit_outside",
 "data": {
  "name": "Image"
 },
 "shadow": false
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_062A3830_1140_E215_4195_1698933FE51C",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 60,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "children": [
  "this.HTMLText_062AD830_1140_E215_41B0_321699661E7F",
  "this.Button_062AF830_1140_E215_418D_D2FC11B12C47"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_062A2830_1140_E215_41AA_EB25B7BD381C",
 "width": "100%",
 "paddingBottom": 30,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 520,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#E73B2C",
 "minWidth": 100,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.79,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container text"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_062AE830_1140_E215_4180_196ED689F4BD",
 "width": 370,
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "height": 40,
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "progressBorderRadius": 0,
 "toolTipPaddingRight": 6,
 "id": "ViewerAreaLabeled_23F787B7_0C0A_6293_419A_B4B58B92DAFC",
 "left": 0,
 "paddingBottom": 0,
 "toolTipPaddingTop": 4,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "right": 0,
 "toolTipPaddingLeft": 6,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "toolTipDisplayTime": 600,
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadHeight": 15,
 "minHeight": 1,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBorderColor": "#FFFFFF",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "playbackBarBottom": 0,
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 1,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 1,
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": 12,
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "toolTipShadowHorizontalLength": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": false,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "top": 0,
 "playbackBarHeadShadowColor": "#000000",
 "bottom": 0,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "vrPointerSelectionTime": 2000,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "toolTipShadowVerticalLength": 0,
 "playbackBarHeadShadow": true,
 "progressBottom": 2,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "Viewer info 1"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "children": [
  "this.IconButton_23F7E7B7_0C0A_6293_419F_D3D84EB3AFBD",
  "this.Container_23F7D7B7_0C0A_6293_4195_312C9CAEABE4",
  "this.IconButton_23F037B7_0C0A_6293_41A2_C1707EE666E4"
 ],
 "id": "Container_23F7F7B7_0C0A_6293_4195_D6240EBAFDC0",
 "left": "0%",
 "width": "100%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "backgroundOpacity": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "height": "100%",
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container arrows"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_23F017B8_0C0A_629D_41A5_DE420F5F9331",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 60,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "children": [
  "this.HTMLText_23F067B8_0C0A_629D_41A9_1A1C797BB055",
  "this.Button_23F057B8_0C0A_629D_41A2_CD6BDCDB0145"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_23F007B8_0C0A_629D_41A3_034CF0D91203",
 "width": "100%",
 "paddingBottom": 30,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 520,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#E73B2C",
 "minWidth": 100,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.79,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container text"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_23F047B8_0C0A_629D_415D_F05EF8619564",
 "width": 370,
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "height": 40,
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "id": "HTMLText_3918BF37_0C06_E393_41A1_17CF0ADBAB12",
 "left": "0%",
 "width": "77.115%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "backgroundOpacity": 0,
 "paddingRight": 0,
 "paddingLeft": 80,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 100,
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:5.21vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:4.18vh;font-family:'Bebas Neue Bold';\">Panorama list:</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText54192"
 },
 "shadow": false
},
{
 "maxHeight": 60,
 "id": "IconButton_38922473_0C06_2593_4199_C585853A1AB3",
 "width": "100%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 20,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_38922473_0C06_2593_4199_C585853A1AB3_rollover.jpg",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "top",
 "top": 20,
 "minWidth": 50,
 "mode": "push",
 "height": "36.14%",
 "class": "IconButton",
 "horizontalAlign": "right",
 "pressedIconURL": "skin/IconButton_38922473_0C06_2593_4199_C585853A1AB3_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_39DE87B1_0C06_62AF_417B_8CB0FB5C9D15, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_38922473_0C06_2593_4199_C585853A1AB3.jpg",
 "cursor": "hand"
},
{
 "id": "HTMLText_2F8A4686_0D4F_6B71_4183_10C1696E2923",
 "left": "0%",
 "width": "77.115%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "backgroundOpacity": 0,
 "paddingRight": 0,
 "paddingLeft": 80,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 100,
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:4.18vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:4.18vh;font-family:'Bebas Neue Bold';\">LAYOUT PLAN:</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText54192"
 },
 "shadow": false
},
{
 "maxHeight": 60,
 "id": "IconButton_2F8A5686_0D4F_6B71_41A1_13CF877A165E",
 "width": "100%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 20,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_2F8A5686_0D4F_6B71_41A1_13CF877A165E_rollover.jpg",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "top",
 "top": 20,
 "minWidth": 50,
 "mode": "push",
 "height": "36.14%",
 "class": "IconButton",
 "horizontalAlign": "right",
 "pressedIconURL": "skin/IconButton_2F8A5686_0D4F_6B71_41A1_13CF877A165E_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_2F8BB687_0D4F_6B7F_4190_9490D02FBC41, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_2F8A5686_0D4F_6B71_41A1_13CF877A165E.jpg",
 "cursor": "hand"
},
{
 "id": "HTMLText_28217A13_0D5D_5B97_419A_F894ECABEB04",
 "left": "0%",
 "width": "77.115%",
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingBottom": 0,
 "backgroundOpacity": 0,
 "paddingRight": 0,
 "paddingLeft": 80,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 100,
 "top": "0%",
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:5.21vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:5.21vh;font-family:'Bebas Neue Bold';\">PHOTOALBUM:</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText54192"
 },
 "shadow": false
},
{
 "maxHeight": 60,
 "id": "IconButton_28216A13_0D5D_5B97_41A9_2CAB10DB6CA3",
 "width": "100%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 20,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_28216A13_0D5D_5B97_41A9_2CAB10DB6CA3_rollover.jpg",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "top",
 "top": 20,
 "minWidth": 50,
 "mode": "push",
 "height": "36.14%",
 "class": "IconButton",
 "horizontalAlign": "right",
 "pressedIconURL": "skin/IconButton_28216A13_0D5D_5B97_41A9_2CAB10DB6CA3_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_2820BA13_0D5D_5B97_4192_AABC38F6F169, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_28216A13_0D5D_5B97_41A9_2CAB10DB6CA3.jpg",
 "cursor": "hand"
},
{
 "toolTipPaddingTop": 4,
 "progressBorderRadius": 0,
 "id": "ViewerAreaLabeled_281D2361_0D5F_E9B0_41A1_A1F237F85FD7",
 "left": "0%",
 "width": "100%",
 "paddingBottom": 0,
 "toolTipPaddingRight": 6,
 "playbackBarProgressBackgroundColorRatios": [
  0
 ],
 "borderRadius": 0,
 "toolTipDisplayTime": 600,
 "toolTipPaddingLeft": 6,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "progressBackgroundColorRatios": [
  0.01
 ],
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "toolTipBorderRadius": 3,
 "paddingLeft": 0,
 "borderSize": 0,
 "playbackBarHeadHeight": 15,
 "minHeight": 1,
 "playbackBarBottom": 0,
 "progressBarBorderColor": "#0066FF",
 "progressBackgroundColorDirection": "vertical",
 "progressBarBackgroundColorRatios": [
  0
 ],
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBorderColor": "#FFFFFF",
 "toolTipBorderColor": "#767676",
 "displayTooltipInTouchScreens": true,
 "minWidth": 1,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "class": "ViewerArea",
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "toolTipOpacity": 1,
 "height": "100%",
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "toolTipFontSize": 12,
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "playbackBarHeight": 10,
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "toolTipTextShadowBlurRadius": 3,
 "playbackBarRight": 0,
 "paddingTop": 0,
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "progressBarBorderRadius": 0,
 "toolTipFontWeight": "normal",
 "shadow": false,
 "show": "this.showPopupMedia(this.window_EAC543AF_C5CD_D9BD_41BE_9B029993F938, this.album_E7AC52A3_C5F3_5BA6_41D4_9EBEEC2CEFE5, this.playList_D32923CB_C6F4_F37B_41E1_0802F49BFCB4, '90%', '90%', false, false)",
 "progressBarBorderSize": 6,
 "playbackBarBorderRadius": 0,
 "transitionDuration": 500,
 "playbackBarHeadShadowVerticalLength": 0,
 "toolTipShadowColor": "#333333",
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "paddingRight": 0,
 "playbackBarBorderSize": 0,
 "toolTipShadowHorizontalLength": 0,
 "playbackBarProgressOpacity": 1,
 "propagateClick": false,
 "playbackBarBackgroundOpacity": 1,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "toolTipTextShadowOpacity": 0,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "top": "0%",
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 2000,
 "progressRight": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "toolTipShadowVerticalLength": 0,
 "transitionMode": "blending",
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 2,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 6,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "playbackBarHeadShadowHorizontalLength": 0,
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "data": {
  "name": "Viewer photoalbum + text 1"
 },
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "toolTipBorderSize": 1
},
{
 "maxHeight": 60,
 "id": "IconButton_2A19CC4C_0D3B_DFF0_41AA_D2AC34177CF1",
 "width": "10%",
 "backgroundOpacity": 0,
 "maxWidth": 60,
 "borderRadius": 0,
 "right": 20,
 "paddingBottom": 0,
 "rollOverIconURL": "skin/IconButton_2A19CC4C_0D3B_DFF0_41AA_D2AC34177CF1_rollover.jpg",
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 50,
 "verticalAlign": "top",
 "top": 20,
 "minWidth": 50,
 "mode": "push",
 "height": "10%",
 "class": "IconButton",
 "horizontalAlign": "right",
 "pressedIconURL": "skin/IconButton_2A19CC4C_0D3B_DFF0_41AA_D2AC34177CF1_pressed.jpg",
 "click": "this.setComponentVisibility(this.Container_2A1A5C4D_0D3B_DFF0_41A9_8FC811D03C8E, false, 0, null, null, false)",
 "transparencyActive": false,
 "paddingTop": 0,
 "data": {
  "name": "IconButton X"
 },
 "shadow": false,
 "iconURL": "skin/IconButton_2A19CC4C_0D3B_DFF0_41AA_D2AC34177CF1.jpg",
 "cursor": "hand"
},
{
 "maxHeight": 1000,
 "id": "Image_06C5BBA5_1140_A63F_41A7_E6D01D4CC397",
 "left": "0%",
 "width": "100%",
 "backgroundOpacity": 0,
 "borderRadius": 0,
 "paddingBottom": 0,
 "maxWidth": 2000,
 "paddingRight": 0,
 "verticalAlign": "bottom",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "url": "skin/Image_06C5BBA5_1140_A63F_41A7_E6D01D4CC397.jpg",
 "minHeight": 1,
 "top": "0%",
 "minWidth": 1,
 "height": "100%",
 "class": "Image",
 "horizontalAlign": "center",
 "paddingTop": 0,
 "scaleMode": "fit_outside",
 "data": {
  "name": "Image"
 },
 "shadow": false
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_06C59BA5_1140_A63F_41B1_4B41E3B7D98D",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 0,
 "overflow": "scroll",
 "backgroundColorRatios": [
  0,
  1
 ],
 "height": 60,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "right",
 "gap": 0,
 "backgroundColorDirection": "vertical",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 20,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "children": [
  "this.HTMLText_0B42C466_11C0_623D_4193_9FAB57A5AC33",
  "this.Container_0D9BF47A_11C0_E215_41A4_A63C8527FF9C"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_06C46BA5_1140_A63F_4151_B5A20B4EA86A",
 "width": "100%",
 "paddingBottom": 30,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 520,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#E73B2C",
 "minWidth": 100,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.79,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container text"
 },
 "shadow": false,
 "layout": "vertical"
},
{
 "backgroundOpacity": 0.3,
 "id": "Container_06C42BA5_1140_A63F_4195_037A0687532F",
 "width": 370,
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "overflow": "scroll",
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "height": 40,
 "scrollBarMargin": 2,
 "scrollBarOpacity": 0.5,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container space"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "backgroundOpacity": 0,
 "scrollBarWidth": 10,
 "width": "100%",
 "id": "HTMLText_062AD830_1140_E215_41B0_321699661E7F",
 "borderRadius": 0,
 "paddingBottom": 20,
 "paddingRight": 10,
 "paddingLeft": 10,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "scrollBarColor": "#04A3E1",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:7.66vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:6.71vh;font-family:'Bebas Neue Bold';\">Lorem ipsum</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:6.71vh;font-family:'Bebas Neue Bold';\">dolor sit amet</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:3.31vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:3.31vh;font-family:'Bebas Neue Bold';\">consectetur adipiscing elit. Morbi bibendum pharetra lorem, accumsan san nulla.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\">Mauris aliquet neque quis libero consequat vestibulum. Donec lacinia consequat dolor viverra sagittis. Praesent consequat porttitor risus, eu condimentum nunc. Proin et velit ac sapien luctus efficitur egestas ac augue. Nunc dictum, augue eget eleifend interdum, quam libero imperdiet lectus, vel scelerisque turpis lectus vel ligula. Duis a porta sem. Maecenas sollicitudin nunc id risus fringilla, a pharetra orci iaculis. Aliquam turpis ligula, tincidunt sit amet consequat ac, imperdiet non dolor.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\">Integer gravida dui quis euismod placerat. Maecenas quis accumsan ipsum. Aliquam gravida velit at dolor mollis, quis luctus mauris vulputate. Proin condimentum id nunc sed sollicitudin.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:2.53vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:2.53vh;font-family:'Bebas Neue Bold';\"><B>Donec feugiat:</B></SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Nisl nec mi sollicitudin facilisis </SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Nam sed faucibus est.</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Ut eget lorem sed leo.</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Sollicitudin tempor sit amet non urna. </SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Aliquam feugiat mauris sit amet.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:2.53vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:2.53vh;font-family:'Bebas Neue Bold';\"><B>lorem ipsum:</B></SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:3.47vh;font-family:'Bebas Neue Bold';\"><B>$150,000</B></SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText"
 },
 "shadow": false
},
{
 "textDecoration": "none",
 "fontFamily": "Bebas Neue Bold",
 "rollOverBackgroundOpacity": 1,
 "backgroundOpacity": 0.7,
 "id": "Button_062AF830_1140_E215_418D_D2FC11B12C47",
 "iconHeight": 32,
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "width": "46%",
 "iconWidth": 32,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "pressedBackgroundColorRatios": [
  0
 ],
 "pressedBackgroundOpacity": 1,
 "backgroundColorRatios": [
  0
 ],
 "borderColor": "#000000",
 "iconBeforeLabel": true,
 "minWidth": 1,
 "mode": "push",
 "backgroundColor": [
  "#04A3E1"
 ],
 "fontSize": "3vh",
 "class": "Button",
 "horizontalAlign": "center",
 "gap": 5,
 "backgroundColorDirection": "vertical",
 "height": "9%",
 "label": "lorem ipsum",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "layout": "horizontal",
 "paddingTop": 0,
 "data": {
  "name": "Button"
 },
 "pressedBackgroundColor": [
  "#000000"
 ],
 "shadow": false,
 "fontWeight": "normal",
 "shadowBlurRadius": 6,
 "shadowSpread": 1,
 "cursor": "hand"
},
{
 "backgroundOpacity": 0,
 "scrollBarWidth": 10,
 "width": "80%",
 "id": "Container_23F7D7B7_0C0A_6293_4195_312C9CAEABE4",
 "borderRadius": 0,
 "paddingBottom": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "height": "30%",
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Container separator"
 },
 "shadow": false,
 "layout": "absolute"
},
{
 "backgroundOpacity": 0,
 "scrollBarWidth": 10,
 "width": "100%",
 "id": "HTMLText_23F067B8_0C0A_629D_41A9_1A1C797BB055",
 "borderRadius": 0,
 "paddingBottom": 20,
 "paddingRight": 10,
 "paddingLeft": 10,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "scrollBarColor": "#04A3E1",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:7.66vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:6.71vh;font-family:'Bebas Neue Bold';\">Lorem ipsum</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:6.71vh;font-family:'Bebas Neue Bold';\">dolor sit amet</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:3.31vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:3.31vh;font-family:'Bebas Neue Bold';\">consectetur adipiscing elit. Morbi bibendum pharetra lorem, accumsan san nulla.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\">Mauris aliquet neque quis libero consequat vestibulum. Donec lacinia consequat dolor viverra sagittis. Praesent consequat porttitor risus, eu condimentum nunc. Proin et velit ac sapien luctus efficitur egestas ac augue. Nunc dictum, augue eget eleifend interdum, quam libero imperdiet lectus, vel scelerisque turpis lectus vel ligula. Duis a porta sem. Maecenas sollicitudin nunc id risus fringilla, a pharetra orci iaculis. Aliquam turpis ligula, tincidunt sit amet consequat ac, imperdiet non dolor.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\">Integer gravida dui quis euismod placerat. Maecenas quis accumsan ipsum. Aliquam gravida velit at dolor mollis, quis luctus mauris vulputate. Proin condimentum id nunc sed sollicitudin.</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:2.53vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:2.53vh;font-family:'Bebas Neue Bold';\"><B>Donec feugiat:</B></SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Nisl nec mi sollicitudin facilisis </SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Nam sed faucibus est.</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Ut eget lorem sed leo.</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Sollicitudin tempor sit amet non urna. </SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\"> \u2022 Aliquam feugiat mauris sit amet.</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText"
 },
 "shadow": false
},
{
 "textDecoration": "none",
 "fontFamily": "Bebas Neue Bold",
 "rollOverBackgroundOpacity": 1,
 "backgroundOpacity": 0.7,
 "id": "Button_23F057B8_0C0A_629D_41A2_CD6BDCDB0145",
 "iconHeight": 32,
 "paddingBottom": 0,
 "shadowColor": "#000000",
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "middle",
 "width": "46%",
 "iconWidth": 32,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "pressedBackgroundColorRatios": [
  0
 ],
 "pressedBackgroundOpacity": 1,
 "backgroundColorRatios": [
  0
 ],
 "borderColor": "#000000",
 "iconBeforeLabel": true,
 "minWidth": 1,
 "mode": "push",
 "backgroundColor": [
  "#04A3E1"
 ],
 "fontSize": "3vh",
 "class": "Button",
 "horizontalAlign": "center",
 "gap": 5,
 "backgroundColorDirection": "vertical",
 "height": "9%",
 "label": "lorem ipsum",
 "fontColor": "#FFFFFF",
 "fontStyle": "normal",
 "layout": "horizontal",
 "paddingTop": 0,
 "data": {
  "name": "Button"
 },
 "pressedBackgroundColor": [
  "#000000"
 ],
 "shadow": false,
 "fontWeight": "normal",
 "shadowBlurRadius": 6,
 "shadowSpread": 1,
 "cursor": "hand"
},
{
 "backgroundOpacity": 0,
 "scrollBarWidth": 10,
 "width": "100%",
 "id": "HTMLText_0B42C466_11C0_623D_4193_9FAB57A5AC33",
 "borderRadius": 0,
 "paddingBottom": 10,
 "paddingRight": 0,
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "scrollBarColor": "#04A3E1",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "45%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:7.66vh;font-family:'Bebas Neue Bold';\">___</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:6.08vh;font-family:'Bebas Neue Bold';\">real estate agent</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText18899"
 },
 "shadow": false
},
{
 "children": [
  "this.Image_0B48D65D_11C0_6E0F_41A2_4D6F373BABA0",
  "this.HTMLText_0B4B0DC1_11C0_6277_41A4_201A5BB3F7AE"
 ],
 "backgroundOpacity": 0.3,
 "id": "Container_0D9BF47A_11C0_E215_41A4_A63C8527FF9C",
 "width": "100%",
 "paddingBottom": 0,
 "scrollBarWidth": 10,
 "borderRadius": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "backgroundColorRatios": [
  0,
  1
 ],
 "scrollBarColor": "#000000",
 "minWidth": 1,
 "backgroundColor": [
  "#FFFFFF",
  "#FFFFFF"
 ],
 "scrollBarVisible": "rollOver",
 "class": "Container",
 "horizontalAlign": "left",
 "gap": 10,
 "backgroundColorDirection": "vertical",
 "scrollBarOpacity": 0.5,
 "overflow": "scroll",
 "scrollBarMargin": 2,
 "height": "80%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "- content"
 },
 "shadow": false,
 "layout": "horizontal"
},
{
 "maxHeight": 200,
 "id": "Image_0B48D65D_11C0_6E0F_41A2_4D6F373BABA0",
 "width": "25%",
 "maxWidth": 200,
 "borderRadius": 0,
 "paddingBottom": 0,
 "backgroundOpacity": 0,
 "paddingRight": 0,
 "verticalAlign": "top",
 "paddingLeft": 0,
 "borderSize": 0,
 "propagateClick": false,
 "url": "skin/Image_0B48D65D_11C0_6E0F_41A2_4D6F373BABA0.jpg",
 "minHeight": 1,
 "minWidth": 1,
 "height": "100%",
 "class": "Image",
 "horizontalAlign": "left",
 "paddingTop": 0,
 "scaleMode": "fit_inside",
 "data": {
  "name": "agent photo"
 },
 "shadow": false
},
{
 "backgroundOpacity": 0,
 "scrollBarWidth": 10,
 "width": "75%",
 "id": "HTMLText_0B4B0DC1_11C0_6277_41A4_201A5BB3F7AE",
 "borderRadius": 0,
 "paddingBottom": 10,
 "paddingRight": 10,
 "paddingLeft": 10,
 "borderSize": 0,
 "propagateClick": false,
 "minHeight": 1,
 "scrollBarColor": "#04A3E1",
 "minWidth": 1,
 "scrollBarOpacity": 0.5,
 "scrollBarVisible": "rollOver",
 "class": "HTMLText",
 "height": "100%",
 "html": "<div style=\"text-align:left; color:#000; \"><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#04a3e1;font-size:3.31vh;font-family:'Bebas Neue Bold';\">john doe</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.89vh;font-family:'Bebas Neue Bold';\">licensed real estate salesperson</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.82vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#999999;font-size:1.82vh;font-family:'Bebas Neue Bold';\">Tlf.: +11 111 111 111</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#999999;font-size:1.82vh;font-family:'Bebas Neue Bold';\">jhondoe@realestate.com</SPAN></SPAN></DIV><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"color:#999999;font-size:1.82vh;font-family:'Bebas Neue Bold';\">www.loremipsum.com</SPAN></SPAN></DIV><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><p STYLE=\"margin:0; line-height:1.03vh;\"><BR STYLE=\"letter-spacing:0vh;color:#000000;font-size:1.03vh;font-family:Arial, Helvetica, sans-serif;\"/></p><DIV STYLE=\"text-align:left;\"><SPAN STYLE=\"letter-spacing:0vh;color:#000000;font-family:Arial, Helvetica, sans-serif;\"><SPAN STYLE=\"font-size:1.03vh;\">Mauris aliquet neque quis libero consequat vestibulum. Donec lacinia consequat dolor viverra sagittis. Praesent consequat porttitor risus, eu condimentum nunc. Proin et velit ac sapien luctus efficitur egestas ac augue. Nunc dictum, augue eget eleifend interdum, quam libero imperdiet lectus, vel scelerisque turpis lectus vel ligula. Duis a porta sem. Maecenas sollicitudin nunc id risus fringilla, a pharetra orci iaculis. Aliquam turpis ligula, tincidunt sit amet consequat ac, imperdiet non dolor.</SPAN></SPAN></DIV></div>",
 "scrollBarMargin": 2,
 "paddingTop": 0,
 "data": {
  "name": "HTMLText19460"
 },
 "shadow": false
}],
 "desktopMipmappingEnabled": false,
 "mobileMipmappingEnabled": false,
 "scrollBarColor": "#000000",
 "minWidth": 20,
 "scrollBarVisible": "rollOver",
 "class": "Player",
 "horizontalAlign": "left",
 "gap": 10,
 "vrPolyfillScale": 1,
 "scrollBarOpacity": 0.5,
 "overflow": "visible",
 "scrollBarMargin": 2,
 "height": "100%",
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Player468"
 },
 "mouseWheelEnabled": true,
 "shadow": false,
 "layout": "absolute",
 "downloadEnabled": false,
 "defaultVRPointer": "laser",
 "buttonToggleFullscreen": [
  "this.IconButton_EEFF957A_E389_9A06_41E1_2AD21904F8C0",
  "this.IconButton_C945391F_C6F3_FF1B_41E2_5C0A7F52D021"
 ],
 "scripts": {
  "getOverlays": function(media){  switch(media.get('class')){ case 'Panorama': var overlays = media.get('overlays').concat() || []; var frames = media.get('frames'); for(var j = 0; j<frames.length; ++j){ overlays = overlays.concat(frames[j].get('overlays') || []); } return overlays; case 'Video360': case 'Map': return media.get('overlays') || []; default: return []; } },
  "unregisterKey": function(key){  delete window[key]; },
  "autotriggerAtStart": function(playList, callback, once){  var onChange = function(event){ callback(); if(once == true) playList.unbind('change', onChange, this); }; playList.bind('change', onChange, this); },
  "pauseGlobalAudiosWhilePlayItem": function(playList, index, exclude){  var self = this; var item = playList.get('items')[index]; var media = item.get('media'); var player = item.get('player'); var caller = media.get('id'); var endFunc = function(){ if(playList.get('selectedIndex') != index) { if(hasState){ player.unbind('stateChange', stateChangeFunc, self); } self.resumeGlobalAudios(caller); } }; var stateChangeFunc = function(event){ var state = event.data.state; if(state == 'stopped'){ this.resumeGlobalAudios(caller); } else if(state == 'playing'){ this.pauseGlobalAudios(caller, exclude); } }; var mediaClass = media.get('class'); var hasState = mediaClass == 'Video360' || mediaClass == 'Video'; if(hasState){ player.bind('stateChange', stateChangeFunc, this); } this.pauseGlobalAudios(caller, exclude); this.executeFunctionWhenChange(playList, index, endFunc, endFunc); },
  "playGlobalAudioWhilePlay": function(playList, index, audio, endCallback){  var changeFunction = function(event){ if(event.data.previousSelectedIndex == index){ this.stopGlobalAudio(audio); if(isPanorama) { var media = playListItem.get('media'); var audios = media.get('audios'); audios.splice(audios.indexOf(audio), 1); media.set('audios', audios); } playList.unbind('change', changeFunction, this); if(endCallback) endCallback(); } }; var audios = window.currentGlobalAudios; if(audios && audio.get('id') in audios){ audio = audios[audio.get('id')]; if(audio.get('state') != 'playing'){ audio.play(); } return audio; } playList.bind('change', changeFunction, this); var playListItem = playList.get('items')[index]; var isPanorama = playListItem.get('class') == 'PanoramaPlayListItem'; if(isPanorama) { var media = playListItem.get('media'); var audios = (media.get('audios') || []).slice(); if(audio.get('class') == 'MediaAudio') { var panoramaAudio = this.rootPlayer.createInstance('PanoramaAudio'); panoramaAudio.set('autoplay', false); panoramaAudio.set('audio', audio.get('audio')); panoramaAudio.set('loop', audio.get('loop')); panoramaAudio.set('id', audio.get('id')); var stateChangeFunctions = audio.getBindings('stateChange'); for(var i = 0; i<stateChangeFunctions.length; ++i){ var f = stateChangeFunctions[i]; if(typeof f == 'string') f = new Function('event', f); panoramaAudio.bind('stateChange', f, this); } audio = panoramaAudio; } audios.push(audio); media.set('audios', audios); } return this.playGlobalAudio(audio, endCallback); },
  "registerKey": function(key, value){  window[key] = value; },
  "showWindow": function(w, autoCloseMilliSeconds, containsAudio){  if(w.get('visible') == true){ return; } var closeFunction = function(){ clearAutoClose(); this.resumePlayers(playersPaused, !containsAudio); w.unbind('close', closeFunction, this); }; var clearAutoClose = function(){ w.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ w.hide(); }; w.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } var playersPaused = this.pauseCurrentPlayers(!containsAudio); w.bind('close', closeFunction, this); w.show(this, true); },
  "getPlayListWithMedia": function(media, onlySelected){  var playLists = this.getByClassName('PlayList'); for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(onlySelected && playList.get('selectedIndex') == -1) continue; if(this.getPlayListItemByMedia(playList, media) != undefined) return playList; } return undefined; },
  "loadFromCurrentMediaPlayList": function(playList, delta){  var currentIndex = playList.get('selectedIndex'); var totalItems = playList.get('items').length; var newIndex = (currentIndex + delta) % totalItems; while(newIndex < 0){ newIndex = totalItems + newIndex; }; if(currentIndex != newIndex){ playList.set('selectedIndex', newIndex); } },
  "getMediaWidth": function(media){  switch(media.get('class')){ case 'Video360': var res = media.get('video'); if(res instanceof Array){ var maxW=0; for(var i=0; i<res.length; i++){ var r = res[i]; if(r.get('width') > maxW) maxW = r.get('width'); } return maxW; }else{ return r.get('width') } default: return media.get('width'); } },
  "pauseGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; } if(audio.get('state') == 'playing') audio.pause(); },
  "showComponentsWhileMouseOver": function(parentComponent, components, durationVisibleWhileOut){  var setVisibility = function(visible){ for(var i = 0, length = components.length; i<length; i++){ var component = components[i]; if(component.get('class') == 'HTMLText' && (component.get('html') == '' || component.get('html') == undefined)) { continue; } component.set('visible', visible); } }; if (this.rootPlayer.get('touchDevice') == true){ setVisibility(true); } else { var timeoutID = -1; var rollOverFunction = function(){ setVisibility(true); if(timeoutID >= 0) clearTimeout(timeoutID); parentComponent.unbind('rollOver', rollOverFunction, this); parentComponent.bind('rollOut', rollOutFunction, this); }; var rollOutFunction = function(){ var timeoutFunction = function(){ setVisibility(false); parentComponent.unbind('rollOver', rollOverFunction, this); }; parentComponent.unbind('rollOut', rollOutFunction, this); parentComponent.bind('rollOver', rollOverFunction, this); timeoutID = setTimeout(timeoutFunction, durationVisibleWhileOut); }; parentComponent.bind('rollOver', rollOverFunction, this); } },
  "executeFunctionWhenChange": function(playList, index, endFunction, changeFunction){  var endObject = undefined; var changePlayListFunction = function(event){ if(event.data.previousSelectedIndex == index){ if(changeFunction) changeFunction.call(this); if(endFunction && endObject) endObject.unbind('end', endFunction, this); playList.unbind('change', changePlayListFunction, this); } }; if(endFunction){ var playListItem = playList.get('items')[index]; if(playListItem.get('class') == 'PanoramaPlayListItem'){ var camera = playListItem.get('camera'); if(camera != undefined) endObject = camera.get('initialSequence'); if(endObject == undefined) endObject = camera.get('idleSequence'); } else{ endObject = playListItem.get('media'); } if(endObject){ endObject.bind('end', endFunction, this); } } playList.bind('change', changePlayListFunction, this); },
  "pauseCurrentPlayers": function(onlyPauseCameraIfPanorama){  var players = this.getCurrentPlayers(); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('state') == 'playing') { if(onlyPauseCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.pauseCamera(); } else { player.pause(); } } else { players.splice(i, 1); } } return players; },
  "shareTwitter": function(url){  window.open('https://twitter.com/intent/tweet?source=webclient&url=' + url, '_blank'); },
  "historyGoForward": function(playList){  var history = this.get('data')['history'][playList.get('id')]; if(history != undefined) { history.forward(); } },
  "fixTogglePlayPauseButton": function(player){  var state = player.get('state'); var buttons = player.get('buttonPlayPause'); if(typeof buttons !== 'undefined' && player.get('state') == 'playing'){ if(!Array.isArray(buttons)) buttons = [buttons]; for(var i = 0; i<buttons.length; ++i) buttons[i].set('pressed', true); } },
  "stopGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; if(audio){ delete audios[audio.get('id')]; if(Object.keys(audios).length == 0){ window.currentGlobalAudios = undefined; } } } if(audio) audio.stop(); },
  "setMapLocation": function(panoramaPlayListItem, mapPlayer){  var resetFunction = function(){ panoramaPlayListItem.unbind('stop', resetFunction, this); player.set('mapPlayer', null); }; panoramaPlayListItem.bind('stop', resetFunction, this); var player = panoramaPlayListItem.get('player'); player.set('mapPlayer', mapPlayer); },
  "resumePlayers": function(players, onlyResumeCameraIfPanorama){  for(var i = 0; i<players.length; ++i){ var player = players[i]; if(onlyResumeCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.resumeCamera(); } else{ player.play(); } } },
  "getPlayListItems": function(media, player){  var itemClass = (function() { switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': return 'PanoramaPlayListItem'; case 'Video360': return 'Video360PlayListItem'; case 'PhotoAlbum': return 'PhotoAlbumPlayListItem'; case 'Map': return 'MapPlayListItem'; case 'Video': return 'VideoPlayListItem'; } })(); if (itemClass != undefined) { var items = this.getByClassName(itemClass); for (var i = items.length-1; i>=0; --i) { var item = items[i]; if(item.get('media') != media || (player != undefined && item.get('player') != player)) { items.splice(i, 1); } } return items; } else { return []; } },
  "showPopupPanoramaOverlay": function(popupPanoramaOverlay, closeButtonProperties, imageHD, toggleImage, toggleImageHD, autoCloseMilliSeconds, audio, stopBackgroundAudio){  var self = this; this.MainViewer.set('toolTipEnabled', false); var cardboardEnabled = this.isCardboardViewMode(); if(!cardboardEnabled) { var zoomImage = this.zoomImagePopupPanorama; var showDuration = popupPanoramaOverlay.get('showDuration'); var hideDuration = popupPanoramaOverlay.get('hideDuration'); var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); var popupMaxWidthBackup = popupPanoramaOverlay.get('popupMaxWidth'); var popupMaxHeightBackup = popupPanoramaOverlay.get('popupMaxHeight'); var showEndFunction = function() { var loadedFunction = function(){ if(!self.isCardboardViewMode()) popupPanoramaOverlay.set('visible', false); }; popupPanoramaOverlay.unbind('showEnd', showEndFunction, self); popupPanoramaOverlay.set('showDuration', 1); popupPanoramaOverlay.set('hideDuration', 1); self.showPopupImage(imageHD, toggleImageHD, popupPanoramaOverlay.get('popupMaxWidth'), popupPanoramaOverlay.get('popupMaxHeight'), null, null, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedFunction, hideFunction); }; var hideFunction = function() { var restoreShowDurationFunction = function(){ popupPanoramaOverlay.unbind('showEnd', restoreShowDurationFunction, self); popupPanoramaOverlay.set('visible', false); popupPanoramaOverlay.set('showDuration', showDuration); popupPanoramaOverlay.set('popupMaxWidth', popupMaxWidthBackup); popupPanoramaOverlay.set('popupMaxHeight', popupMaxHeightBackup); }; self.resumePlayers(playersPaused, audio == null || !stopBackgroundAudio); var currentWidth = zoomImage.get('imageWidth'); var currentHeight = zoomImage.get('imageHeight'); popupPanoramaOverlay.bind('showEnd', restoreShowDurationFunction, self, true); popupPanoramaOverlay.set('showDuration', 1); popupPanoramaOverlay.set('hideDuration', hideDuration); popupPanoramaOverlay.set('popupMaxWidth', currentWidth); popupPanoramaOverlay.set('popupMaxHeight', currentHeight); if(popupPanoramaOverlay.get('visible')) restoreShowDurationFunction(); else popupPanoramaOverlay.set('visible', true); self.MainViewer.set('toolTipEnabled', true); }; if(!imageHD){ imageHD = popupPanoramaOverlay.get('image'); } if(!toggleImageHD && toggleImage){ toggleImageHD = toggleImage; } popupPanoramaOverlay.bind('showEnd', showEndFunction, this, true); } else { var hideEndFunction = function() { self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ self.resumeGlobalAudios(); } self.stopGlobalAudio(audio); } popupPanoramaOverlay.unbind('hideEnd', hideEndFunction, self); self.MainViewer.set('toolTipEnabled', true); }; var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ this.pauseGlobalAudios(); } this.playGlobalAudio(audio); } popupPanoramaOverlay.bind('hideEnd', hideEndFunction, this, true); } popupPanoramaOverlay.set('visible', true); },
  "resumeGlobalAudios": function(caller){  if (window.pauseGlobalAudiosState == undefined || !(caller in window.pauseGlobalAudiosState)) return; var audiosPaused = window.pauseGlobalAudiosState[caller]; delete window.pauseGlobalAudiosState[caller]; var values = Object.values(window.pauseGlobalAudiosState); for (var i = 0, count = values.length; i<count; ++i) { var objAudios = values[i]; for (var j = audiosPaused.length-1; j>=0; --j) { var a = audiosPaused[j]; if(objAudios.indexOf(a) != -1) audiosPaused.splice(j, 1); } } for (var i = 0, count = audiosPaused.length; i<count; ++i) { var a = audiosPaused[i]; if (a.get('state') == 'paused') a.play(); } },
  "setStartTimeVideoSync": function(video, player){  this.setStartTimeVideo(video, player.get('currentTime')); },
  "getPixels": function(value){  var result = new RegExp('((\\+|\\-)?\\d+(\\.\\d*)?)(px|vw|vh|vmin|vmax)?', 'i').exec(value); if (result == undefined) { return 0; } var num = parseFloat(result[1]); var unit = result[4]; var vw = this.rootPlayer.get('actualWidth') / 100; var vh = this.rootPlayer.get('actualHeight') / 100; switch(unit) { case 'vw': return num * vw; case 'vh': return num * vh; case 'vmin': return num * Math.min(vw, vh); case 'vmax': return num * Math.max(vw, vh); default: return num; } },
  "getKey": function(key){  return window[key]; },
  "shareFacebook": function(url){  window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank'); },
  "init": function(){  if(!Object.hasOwnProperty('values')) { Object.values = function(o){ return Object.keys(o).map(function(e) { return o[e]; }); }; } var history = this.get('data')['history']; var playListChangeFunc = function(e){ var playList = e.source; var index = playList.get('selectedIndex'); if(index < 0) return; var id = playList.get('id'); if(!history.hasOwnProperty(id)) history[id] = new HistoryData(playList); history[id].add(index); }; var playLists = this.getByClassName('PlayList'); for(var i = 0, count = playLists.length; i<count; ++i) { var playList = playLists[i]; playList.bind('change', playListChangeFunc, this); } },
  "triggerOverlay": function(overlay, eventName){  if(overlay.get('areas') != undefined) { var areas = overlay.get('areas'); for(var i = 0; i<areas.length; ++i) { areas[i].trigger(eventName); } } else { overlay.trigger(eventName); } },
  "setCameraSameSpotAsMedia": function(camera, media){  var player = this.getCurrentPlayerWithMedia(media); if(player != undefined) { var position = camera.get('initialPosition'); position.set('yaw', player.get('yaw')); position.set('pitch', player.get('pitch')); position.set('hfov', player.get('hfov')); } },
  "setMainMediaByName": function(name){  var items = this.mainPlayList.get('items'); for(var i = 0; i<items.length; ++i){ var item = items[i]; if(item.get('media').get('label') == name) { this.mainPlayList.set('selectedIndex', i); return item; } } },
  "visibleComponentsIfPlayerFlagEnabled": function(components, playerFlag){  var enabled = this.get(playerFlag); for(var i in components){ components[i].set('visible', enabled); } },
  "startPanoramaWithCamera": function(media, camera){  if(window.currentPanoramasWithCameraChanged != undefined && window.currentPanoramasWithCameraChanged.indexOf(media) != -1){ return; } var playLists = this.getByClassName('PlayList'); if(playLists.length == 0) return; var restoreItems = []; for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ var item = items[j]; if(item.get('media') == media && (item.get('class') == 'PanoramaPlayListItem' || item.get('class') == 'Video360PlayListItem')){ restoreItems.push({camera: item.get('camera'), item: item}); item.set('camera', camera); } } } if(restoreItems.length > 0) { if(window.currentPanoramasWithCameraChanged == undefined) { window.currentPanoramasWithCameraChanged = [media]; } else { window.currentPanoramasWithCameraChanged.push(media); } var restoreCameraOnStop = function(){ var index = window.currentPanoramasWithCameraChanged.indexOf(media); if(index != -1) { window.currentPanoramasWithCameraChanged.splice(index, 1); } for (var i = 0; i < restoreItems.length; i++) { restoreItems[i].item.set('camera', restoreItems[i].camera); restoreItems[i].item.unbind('stop', restoreCameraOnStop, this); } }; for (var i = 0; i < restoreItems.length; i++) { restoreItems[i].item.bind('stop', restoreCameraOnStop, this); } } },
  "getPlayListItemByMedia": function(playList, media){  var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ var item = items[j]; if(item.get('media') == media) return item; } return undefined; },
  "isCardboardViewMode": function(){  var players = this.getByClassName('PanoramaPlayer'); return players.length > 0 && players[0].get('viewMode') == 'cardboard'; },
  "getMediaFromPlayer": function(player){  switch(player.get('class')){ case 'PanoramaPlayer': return player.get('panorama') || player.get('video'); case 'VideoPlayer': case 'Video360Player': return player.get('video'); case 'PhotoAlbumPlayer': return player.get('photoAlbum'); case 'MapPlayer': return player.get('map'); } },
  "updateMediaLabelFromPlayList": function(playList, htmlText, playListItemStopToDispose){  var changeFunction = function(){ var index = playList.get('selectedIndex'); if(index >= 0){ var beginFunction = function(){ playListItem.unbind('begin', beginFunction); setMediaLabel(index); }; var setMediaLabel = function(index){ var media = playListItem.get('media'); var text = media.get('data'); if(!text) text = media.get('label'); setHtml(text); }; var setHtml = function(text){ if(text !== undefined) { htmlText.set('html', '<div style=\"text-align:left\"><SPAN STYLE=\"color:#FFFFFF;font-size:12px;font-family:Verdana\"><span color=\"white\" font-family=\"Verdana\" font-size=\"12px\">' + text + '</SPAN></div>'); } else { htmlText.set('html', ''); } }; var playListItem = playList.get('items')[index]; if(htmlText.get('html')){ setHtml('Loading...'); playListItem.bind('begin', beginFunction); } else{ setMediaLabel(index); } } }; var disposeFunction = function(){ htmlText.set('html', undefined); playList.unbind('change', changeFunction, this); playListItemStopToDispose.unbind('stop', disposeFunction, this); }; if(playListItemStopToDispose){ playListItemStopToDispose.bind('stop', disposeFunction, this); } playList.bind('change', changeFunction, this); changeFunction(); },
  "setMediaBehaviour": function(playList, index, mediaDispatcher){  var self = this; var stateChangeFunction = function(event){ if(event.data.state == 'stopped'){ dispose.call(this, true); } }; var onBeginFunction = function() { item.unbind('begin', onBeginFunction, self); var media = item.get('media'); if(media.get('class') != 'Panorama' || (media.get('camera') != undefined && media.get('camera').get('initialSequence') != undefined)){ player.bind('stateChange', stateChangeFunction, self); } }; var changeFunction = function(){ var index = playListDispatcher.get('selectedIndex'); if(index != -1){ indexDispatcher = index; dispose.call(this, false); } }; var disposeCallback = function(){ dispose.call(this, false); }; var dispose = function(forceDispose){ if(!playListDispatcher) return; var media = item.get('media'); if((media.get('class') == 'Video360' || media.get('class') == 'Video') && media.get('loop') == true && !forceDispose) return; playList.set('selectedIndex', -1); if(panoramaSequence && panoramaSequenceIndex != -1){ if(panoramaSequence) { if(panoramaSequenceIndex > 0 && panoramaSequence.get('movements')[panoramaSequenceIndex-1].get('class') == 'TargetPanoramaCameraMovement'){ var initialPosition = camera.get('initialPosition'); var oldYaw = initialPosition.get('yaw'); var oldPitch = initialPosition.get('pitch'); var oldHfov = initialPosition.get('hfov'); var previousMovement = panoramaSequence.get('movements')[panoramaSequenceIndex-1]; initialPosition.set('yaw', previousMovement.get('targetYaw')); initialPosition.set('pitch', previousMovement.get('targetPitch')); initialPosition.set('hfov', previousMovement.get('targetHfov')); var restoreInitialPositionFunction = function(event){ initialPosition.set('yaw', oldYaw); initialPosition.set('pitch', oldPitch); initialPosition.set('hfov', oldHfov); itemDispatcher.unbind('end', restoreInitialPositionFunction, this); }; itemDispatcher.bind('end', restoreInitialPositionFunction, this); } panoramaSequence.set('movementIndex', panoramaSequenceIndex); } } if(player){ item.unbind('begin', onBeginFunction, this); player.unbind('stateChange', stateChangeFunction, this); for(var i = 0; i<buttons.length; ++i) { buttons[i].unbind('click', disposeCallback, this); } } if(sameViewerArea){ var currentMedia = this.getMediaFromPlayer(player); if(currentMedia == undefined || currentMedia == item.get('media')){ playListDispatcher.set('selectedIndex', indexDispatcher); } if(playList != playListDispatcher) playListDispatcher.unbind('change', changeFunction, this); } else{ viewerArea.set('visible', viewerVisibility); } playListDispatcher = undefined; }; var mediaDispatcherByParam = mediaDispatcher != undefined; if(!mediaDispatcher){ var currentIndex = playList.get('selectedIndex'); var currentPlayer = (currentIndex != -1) ? playList.get('items')[playList.get('selectedIndex')].get('player') : this.getActivePlayerWithViewer(this.MainViewer); if(currentPlayer) { mediaDispatcher = this.getMediaFromPlayer(currentPlayer); } } var playListDispatcher = mediaDispatcher ? this.getPlayListWithMedia(mediaDispatcher, true) : undefined; if(!playListDispatcher){ playList.set('selectedIndex', index); return; } var indexDispatcher = playListDispatcher.get('selectedIndex'); if(playList.get('selectedIndex') == index || indexDispatcher == -1){ return; } var item = playList.get('items')[index]; var itemDispatcher = playListDispatcher.get('items')[indexDispatcher]; var player = item.get('player'); var viewerArea = player.get('viewerArea'); var viewerVisibility = viewerArea.get('visible'); var sameViewerArea = viewerArea == itemDispatcher.get('player').get('viewerArea'); if(sameViewerArea){ if(playList != playListDispatcher){ playListDispatcher.set('selectedIndex', -1); playListDispatcher.bind('change', changeFunction, this); } } else{ viewerArea.set('visible', true); } var panoramaSequenceIndex = -1; var panoramaSequence = undefined; var camera = itemDispatcher.get('camera'); if(camera){ panoramaSequence = camera.get('initialSequence'); if(panoramaSequence) { panoramaSequenceIndex = panoramaSequence.get('movementIndex'); } } playList.set('selectedIndex', index); var buttons = []; var addButtons = function(property){ var value = player.get(property); if(value == undefined) return; if(Array.isArray(value)) buttons = buttons.concat(value); else buttons.push(value); }; addButtons('buttonStop'); for(var i = 0; i<buttons.length; ++i) { buttons[i].bind('click', disposeCallback, this); } if(player != itemDispatcher.get('player') || !mediaDispatcherByParam){ item.bind('begin', onBeginFunction, self); } this.executeFunctionWhenChange(playList, index, disposeCallback); },
  "showPopupMedia": function(w, media, playList, popupMaxWidth, popupMaxHeight, autoCloseWhenFinished, stopAudios){  var self = this; var closeFunction = function(){ playList.set('selectedIndex', -1); self.MainViewer.set('toolTipEnabled', true); if(stopAudios) { self.resumeGlobalAudios(); } this.resumePlayers(playersPaused, !stopAudios); if(isVideo) { this.unbind('resize', resizeFunction, this); } w.unbind('close', closeFunction, this); }; var endFunction = function(){ w.hide(); }; var resizeFunction = function(){ var getWinValue = function(property){ return w.get(property) || 0; }; var parentWidth = self.get('actualWidth'); var parentHeight = self.get('actualHeight'); var mediaWidth = self.getMediaWidth(media); var mediaHeight = self.getMediaHeight(media); var popupMaxWidthNumber = parseFloat(popupMaxWidth) / 100; var popupMaxHeightNumber = parseFloat(popupMaxHeight) / 100; var windowWidth = popupMaxWidthNumber * parentWidth; var windowHeight = popupMaxHeightNumber * parentHeight; var footerHeight = getWinValue('footerHeight'); var headerHeight = getWinValue('headerHeight'); if(!headerHeight) { var closeButtonHeight = getWinValue('closeButtonIconHeight') + getWinValue('closeButtonPaddingTop') + getWinValue('closeButtonPaddingBottom'); var titleHeight = self.getPixels(getWinValue('titleFontSize')) + getWinValue('titlePaddingTop') + getWinValue('titlePaddingBottom'); headerHeight = closeButtonHeight > titleHeight ? closeButtonHeight : titleHeight; headerHeight += getWinValue('headerPaddingTop') + getWinValue('headerPaddingBottom'); } var contentWindowWidth = windowWidth - getWinValue('bodyPaddingLeft') - getWinValue('bodyPaddingRight') - getWinValue('paddingLeft') - getWinValue('paddingRight'); var contentWindowHeight = windowHeight - headerHeight - footerHeight - getWinValue('bodyPaddingTop') - getWinValue('bodyPaddingBottom') - getWinValue('paddingTop') - getWinValue('paddingBottom'); var parentAspectRatio = contentWindowWidth / contentWindowHeight; var mediaAspectRatio = mediaWidth / mediaHeight; if(parentAspectRatio > mediaAspectRatio) { windowWidth = contentWindowHeight * mediaAspectRatio + getWinValue('bodyPaddingLeft') + getWinValue('bodyPaddingRight') + getWinValue('paddingLeft') + getWinValue('paddingRight'); } else { windowHeight = contentWindowWidth / mediaAspectRatio + headerHeight + footerHeight + getWinValue('bodyPaddingTop') + getWinValue('bodyPaddingBottom') + getWinValue('paddingTop') + getWinValue('paddingBottom'); } if(windowWidth > parentWidth * popupMaxWidthNumber) { windowWidth = parentWidth * popupMaxWidthNumber; } if(windowHeight > parentHeight * popupMaxHeightNumber) { windowHeight = parentHeight * popupMaxHeightNumber; } w.set('width', windowWidth); w.set('height', windowHeight); w.set('x', (parentWidth - getWinValue('actualWidth')) * 0.5); w.set('y', (parentHeight - getWinValue('actualHeight')) * 0.5); }; if(autoCloseWhenFinished){ this.executeFunctionWhenChange(playList, 0, endFunction); } var mediaClass = media.get('class'); var isVideo = mediaClass == 'Video' || mediaClass == 'Video360'; playList.set('selectedIndex', 0); if(isVideo){ this.bind('resize', resizeFunction, this); resizeFunction(); playList.get('items')[0].get('player').play(); } else { w.set('width', popupMaxWidth); w.set('height', popupMaxHeight); } this.MainViewer.set('toolTipEnabled', false); if(stopAudios) { this.pauseGlobalAudios(); } var playersPaused = this.pauseCurrentPlayers(!stopAudios); w.bind('close', closeFunction, this); w.show(this, true); },
  "getComponentByName": function(name){  var list = this.getByClassName('UIComponent'); for(var i = 0, count = list.length; i<count; ++i){ var component = list[i]; var data = component.get('data'); if(data != undefined && data.name == name){ return component; } } return undefined; },
  "setMainMediaByIndex": function(index){  var item = undefined; if(index >= 0 && index < this.mainPlayList.get('items').length){ this.mainPlayList.set('selectedIndex', index); item = this.mainPlayList.get('items')[index]; } return item; },
  "getPanoramaOverlayByName": function(panorama, name){  var overlays = this.getOverlays(panorama); for(var i = 0, count = overlays.length; i<count; ++i){ var overlay = overlays[i]; var data = overlay.get('data'); if(data != undefined && data.label == name){ return overlay; } } return undefined; },
  "getMediaHeight": function(media){  switch(media.get('class')){ case 'Video360': var res = media.get('video'); if(res instanceof Array){ var maxH=0; for(var i=0; i<res.length; i++){ var r = res[i]; if(r.get('height') > maxH) maxH = r.get('height'); } return maxH; }else{ return r.get('height') } default: return media.get('height'); } },
  "keepComponentVisibility": function(component, keep){  var key = 'keepVisibility_' + component.get('id'); var value = this.getKey(key); if(value == undefined && keep) { this.registerKey(key, keep); } else if(value != undefined && !keep) { this.unregisterKey(key); } },
  "setOverlayBehaviour": function(overlay, media, action){  var executeFunc = function() { switch(action){ case 'triggerClick': this.triggerOverlay(overlay, 'click'); break; case 'stop': case 'play': case 'pause': overlay[action](); break; case 'togglePlayPause': case 'togglePlayStop': if(overlay.get('state') == 'playing') overlay[action == 'togglePlayPause' ? 'pause' : 'stop'](); else overlay.play(); break; } if(window.overlaysDispatched == undefined) window.overlaysDispatched = {}; var id = overlay.get('id'); window.overlaysDispatched[id] = true; setTimeout(function(){ delete window.overlaysDispatched[id]; }, 2000); }; if(window.overlaysDispatched != undefined && overlay.get('id') in window.overlaysDispatched) return; var playList = this.getPlayListWithMedia(media, true); if(playList != undefined){ var item = this.getPlayListItemByMedia(playList, media); if(playList.get('items').indexOf(item) != playList.get('selectedIndex')){ var beginFunc = function(e){ item.unbind('begin', beginFunc, this); executeFunc.call(this); }; item.bind('begin', beginFunc, this); return; } } executeFunc.call(this); },
  "cloneCamera": function(camera){  var newCamera = this.rootPlayer.createInstance(camera.get('class')); newCamera.set('id', camera.get('id') + '_copy'); newCamera.set('idleSequence', camera.get('initialSequence')); return newCamera; },
  "setPanoramaCameraWithSpot": function(playListItem, yaw, pitch){  var panorama = playListItem.get('media'); var newCamera = this.cloneCamera(playListItem.get('camera')); var initialPosition = newCamera.get('initialPosition'); initialPosition.set('yaw', yaw); initialPosition.set('pitch', pitch); this.startPanoramaWithCamera(panorama, newCamera); },
  "changeBackgroundWhilePlay": function(playList, index, color){  var stopFunction = function(event){ playListItem.unbind('stop', stopFunction, this); if((color == viewerArea.get('backgroundColor')) && (colorRatios == viewerArea.get('backgroundColorRatios'))){ viewerArea.set('backgroundColor', backgroundColorBackup); viewerArea.set('backgroundColorRatios', backgroundColorRatiosBackup); } }; var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var viewerArea = player.get('viewerArea'); var backgroundColorBackup = viewerArea.get('backgroundColor'); var backgroundColorRatiosBackup = viewerArea.get('backgroundColorRatios'); var colorRatios = [0]; if((color != backgroundColorBackup) || (colorRatios != backgroundColorRatiosBackup)){ viewerArea.set('backgroundColor', color); viewerArea.set('backgroundColorRatios', colorRatios); playListItem.bind('stop', stopFunction, this); } },
  "getCurrentPlayerWithMedia": function(media){  var playerClass = undefined; var mediaPropertyName = undefined; switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'panorama'; break; case 'Video360': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'video'; break; case 'PhotoAlbum': playerClass = 'PhotoAlbumPlayer'; mediaPropertyName = 'photoAlbum'; break; case 'Map': playerClass = 'MapPlayer'; mediaPropertyName = 'map'; break; case 'Video': playerClass = 'VideoPlayer'; mediaPropertyName = 'video'; break; }; if(playerClass != undefined) { var players = this.getByClassName(playerClass); for(var i = 0; i<players.length; ++i){ var player = players[i]; if(player.get(mediaPropertyName) == media) { return player; } } } else { return undefined; } },
  "getActivePlayerWithViewer": function(viewerArea){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); players = players.concat(this.getByClassName('MapPlayer')); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('viewerArea') == viewerArea) { var playerClass = player.get('class'); if(playerClass == 'PanoramaPlayer' && (player.get('panorama') != undefined || player.get('video') != undefined)) return player; else if((playerClass == 'VideoPlayer' || playerClass == 'Video360Player') && player.get('video') != undefined) return player; else if(playerClass == 'PhotoAlbumPlayer' && player.get('photoAlbum') != undefined) return player; else if(playerClass == 'MapPlayer' && player.get('map') != undefined) return player; } } return undefined; },
  "setComponentVisibility": function(component, visible, applyAt, effect, propertyEffect, ignoreClearTimeout){  var keepVisibility = this.getKey('keepVisibility_' + component.get('id')); if(keepVisibility) return; this.unregisterKey('visibility_'+component.get('id')); var changeVisibility = function(){ if(effect && propertyEffect){ component.set(propertyEffect, effect); } component.set('visible', visible); if(component.get('class') == 'ViewerArea'){ try{ if(visible) component.restart(); else if(component.get('playbackState') == 'playing') component.pause(); } catch(e){}; } }; var effectTimeoutName = 'effectTimeout_'+component.get('id'); if(!ignoreClearTimeout && window.hasOwnProperty(effectTimeoutName)){ var effectTimeout = window[effectTimeoutName]; if(effectTimeout instanceof Array){ for(var i=0; i<effectTimeout.length; i++){ clearTimeout(effectTimeout[i]) } }else{ clearTimeout(effectTimeout); } delete window[effectTimeoutName]; } else if(visible == component.get('visible') && !ignoreClearTimeout) return; if(applyAt && applyAt > 0){ var effectTimeout = setTimeout(function(){ if(window[effectTimeoutName] instanceof Array) { var arrayTimeoutVal = window[effectTimeoutName]; var index = arrayTimeoutVal.indexOf(effectTimeout); arrayTimeoutVal.splice(index, 1); if(arrayTimeoutVal.length == 0){ delete window[effectTimeoutName]; } }else{ delete window[effectTimeoutName]; } changeVisibility(); }, applyAt); if(window.hasOwnProperty(effectTimeoutName)){ window[effectTimeoutName] = [window[effectTimeoutName], effectTimeout]; }else{ window[effectTimeoutName] = effectTimeout; } } else{ changeVisibility(); } },
  "setEndToItemIndex": function(playList, fromIndex, toIndex){  var endFunction = function(){ if(playList.get('selectedIndex') == fromIndex) playList.set('selectedIndex', toIndex); }; this.executeFunctionWhenChange(playList, fromIndex, endFunction); },
  "historyGoBack": function(playList){  var history = this.get('data')['history'][playList.get('id')]; if(history != undefined) { history.back(); } },
  "updateVideoCues": function(playList, index){  var playListItem = playList.get('items')[index]; var video = playListItem.get('media'); if(video.get('cues').length == 0) return; var player = playListItem.get('player'); var cues = []; var changeFunction = function(){ if(playList.get('selectedIndex') != index){ video.unbind('cueChange', cueChangeFunction, this); playList.unbind('change', changeFunction, this); } }; var cueChangeFunction = function(event){ var activeCues = event.data.activeCues; for(var i = 0, count = cues.length; i<count; ++i){ var cue = cues[i]; if(activeCues.indexOf(cue) == -1 && (cue.get('startTime') > player.get('currentTime') || cue.get('endTime') < player.get('currentTime')+0.5)){ cue.trigger('end'); } } cues = activeCues; }; video.bind('cueChange', cueChangeFunction, this); playList.bind('change', changeFunction, this); },
  "setStartTimeVideo": function(video, time){  var items = this.getPlayListItems(video); var startTimeBackup = []; var restoreStartTimeFunc = function() { for(var i = 0; i<items.length; ++i){ var item = items[i]; item.set('startTime', startTimeBackup[i]); item.unbind('stop', restoreStartTimeFunc, this); } }; for(var i = 0; i<items.length; ++i) { var item = items[i]; var player = item.get('player'); if(player.get('video') == video && player.get('state') == 'playing') { player.seek(time); } else { startTimeBackup.push(item.get('startTime')); item.set('startTime', time); item.bind('stop', restoreStartTimeFunc, this); } } },
  "getMediaByName": function(name){  var list = this.getByClassName('Media'); for(var i = 0, count = list.length; i<count; ++i){ var media = list[i]; if((media.get('class') == 'Audio' && media.get('data').label == name) || media.get('label') == name){ return media; } } return undefined; },
  "changePlayListWithSameSpot": function(playList, newIndex){  var currentIndex = playList.get('selectedIndex'); if (currentIndex >= 0 && newIndex >= 0 && currentIndex != newIndex) { var currentItem = playList.get('items')[currentIndex]; var newItem = playList.get('items')[newIndex]; var currentPlayer = currentItem.get('player'); var newPlayer = newItem.get('player'); if ((currentPlayer.get('class') == 'PanoramaPlayer' || currentPlayer.get('class') == 'Video360Player') && (newPlayer.get('class') == 'PanoramaPlayer' || newPlayer.get('class') == 'Video360Player')) { var newCamera = this.cloneCamera(newItem.get('camera')); this.setCameraSameSpotAsMedia(newCamera, currentItem.get('media')); this.startPanoramaWithCamera(newItem.get('media'), newCamera); } } },
  "setPanoramaCameraWithCurrentSpot": function(playListItem){  var currentPlayer = this.getActivePlayerWithViewer(this.MainViewer); if(currentPlayer == undefined){ return; } var playerClass = currentPlayer.get('class'); if(playerClass != 'PanoramaPlayer' && playerClass != 'Video360Player'){ return; } var fromMedia = currentPlayer.get('panorama'); if(fromMedia == undefined) { fromMedia = currentPlayer.get('video'); } var panorama = playListItem.get('media'); var newCamera = this.cloneCamera(playListItem.get('camera')); this.setCameraSameSpotAsMedia(newCamera, fromMedia); this.startPanoramaWithCamera(panorama, newCamera); },
  "getGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios != undefined && audio.get('id') in audios){ audio = audios[audio.get('id')]; } return audio; },
  "showPopupPanoramaVideoOverlay": function(popupPanoramaOverlay, closeButtonProperties, stopAudios){  var self = this; var showEndFunction = function() { popupPanoramaOverlay.unbind('showEnd', showEndFunction); closeButton.bind('click', hideFunction, this); setCloseButtonPosition(); closeButton.set('visible', true); }; var endFunction = function() { if(!popupPanoramaOverlay.get('loop')) hideFunction(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); popupPanoramaOverlay.set('visible', false); closeButton.set('visible', false); closeButton.unbind('click', hideFunction, self); popupPanoramaOverlay.unbind('end', endFunction, self); popupPanoramaOverlay.unbind('hideEnd', hideFunction, self, true); self.resumePlayers(playersPaused, true); if(stopAudios) { self.resumeGlobalAudios(); } }; var setCloseButtonPosition = function() { var right = 10; var top = 10; closeButton.set('right', right); closeButton.set('top', top); }; this.MainViewer.set('toolTipEnabled', false); var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(true); if(stopAudios) { this.pauseGlobalAudios(); } popupPanoramaOverlay.bind('end', endFunction, this, true); popupPanoramaOverlay.bind('showEnd', showEndFunction, this, true); popupPanoramaOverlay.bind('hideEnd', hideFunction, this, true); popupPanoramaOverlay.set('visible', true); },
  "shareWhatsapp": function(url){  window.open('https://api.whatsapp.com/send/?text=' + encodeURIComponent(url), '_blank'); },
  "getCurrentPlayers": function(){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); return players; },
  "showPopupImage": function(image, toggleImage, customWidth, customHeight, showEffect, hideEffect, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedCallback, hideCallback){  var self = this; var closed = false; var playerClickFunction = function() { zoomImage.unbind('loaded', loadedFunction, self); hideFunction(); }; var clearAutoClose = function(){ zoomImage.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var resizeFunction = function(){ setTimeout(setCloseButtonPosition, 0); }; var loadedFunction = function(){ self.unbind('click', playerClickFunction, self); veil.set('visible', true); setCloseButtonPosition(); closeButton.set('visible', true); zoomImage.unbind('loaded', loadedFunction, this); zoomImage.bind('userInteractionStart', userInteractionStartFunction, this); zoomImage.bind('userInteractionEnd', userInteractionEndFunction, this); zoomImage.bind('resize', resizeFunction, this); timeoutID = setTimeout(timeoutFunction, 200); }; var timeoutFunction = function(){ timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ hideFunction(); }; zoomImage.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } zoomImage.bind('backgroundClick', hideFunction, this); if(toggleImage) { zoomImage.bind('click', toggleFunction, this); zoomImage.set('imageCursor', 'hand'); } closeButton.bind('click', hideFunction, this); if(loadedCallback) loadedCallback(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); closed = true; if(timeoutID) clearTimeout(timeoutID); if (timeoutUserInteractionID) clearTimeout(timeoutUserInteractionID); if(autoCloseMilliSeconds) clearAutoClose(); if(hideCallback) hideCallback(); zoomImage.set('visible', false); if(hideEffect && hideEffect.get('duration') > 0){ hideEffect.bind('end', endEffectFunction, this); } else{ zoomImage.set('image', null); } closeButton.set('visible', false); veil.set('visible', false); self.unbind('click', playerClickFunction, self); zoomImage.unbind('backgroundClick', hideFunction, this); zoomImage.unbind('userInteractionStart', userInteractionStartFunction, this); zoomImage.unbind('userInteractionEnd', userInteractionEndFunction, this, true); zoomImage.unbind('resize', resizeFunction, this); if(toggleImage) { zoomImage.unbind('click', toggleFunction, this); zoomImage.set('cursor', 'default'); } closeButton.unbind('click', hideFunction, this); self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ self.resumeGlobalAudios(); } self.stopGlobalAudio(audio); } }; var endEffectFunction = function() { zoomImage.set('image', null); hideEffect.unbind('end', endEffectFunction, this); }; var toggleFunction = function() { zoomImage.set('image', isToggleVisible() ? image : toggleImage); }; var isToggleVisible = function() { return zoomImage.get('image') == toggleImage; }; var setCloseButtonPosition = function() { var right = zoomImage.get('actualWidth') - zoomImage.get('imageLeft') - zoomImage.get('imageWidth') + 10; var top = zoomImage.get('imageTop') + 10; if(right < 10) right = 10; if(top < 10) top = 10; closeButton.set('right', right); closeButton.set('top', top); }; var userInteractionStartFunction = function() { if(timeoutUserInteractionID){ clearTimeout(timeoutUserInteractionID); timeoutUserInteractionID = undefined; } else{ closeButton.set('visible', false); } }; var userInteractionEndFunction = function() { if(!closed){ timeoutUserInteractionID = setTimeout(userInteractionTimeoutFunction, 300); } }; var userInteractionTimeoutFunction = function() { timeoutUserInteractionID = undefined; closeButton.set('visible', true); setCloseButtonPosition(); }; this.MainViewer.set('toolTipEnabled', false); var veil = this.veilPopupPanorama; var zoomImage = this.zoomImagePopupPanorama; var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ this.pauseGlobalAudios(); } this.playGlobalAudio(audio); } var timeoutID = undefined; var timeoutUserInteractionID = undefined; zoomImage.bind('loaded', loadedFunction, this); setTimeout(function(){ self.bind('click', playerClickFunction, self, false); }, 0); zoomImage.set('image', image); zoomImage.set('customWidth', customWidth); zoomImage.set('customHeight', customHeight); zoomImage.set('showEffect', showEffect); zoomImage.set('hideEffect', hideEffect); zoomImage.set('visible', true); return zoomImage; },
  "initGA": function(){  var sendFunc = function(category, event, label) { ga('send', 'event', category, event, label); }; var media = this.getByClassName('Panorama'); media = media.concat(this.getByClassName('Video360')); media = media.concat(this.getByClassName('Map')); for(var i = 0, countI = media.length; i<countI; ++i){ var m = media[i]; var mediaLabel = m.get('label'); var overlays = this.getOverlays(m); for(var j = 0, countJ = overlays.length; j<countJ; ++j){ var overlay = overlays[j]; var overlayLabel = overlay.get('data') != undefined ? mediaLabel + ' - ' + overlay.get('data')['label'] : mediaLabel; switch(overlay.get('class')) { case 'HotspotPanoramaOverlay': case 'HotspotMapOverlay': var areas = overlay.get('areas'); for (var z = 0; z<areas.length; ++z) { areas[z].bind('click', sendFunc.bind(this, 'Hotspot', 'click', overlayLabel), this); } break; case 'CeilingCapPanoramaOverlay': case 'TripodCapPanoramaOverlay': overlay.bind('click', sendFunc.bind(this, 'Cap', 'click', overlayLabel), this); break; } } } var components = this.getByClassName('Button'); components = components.concat(this.getByClassName('IconButton')); for(var i = 0, countI = components.length; i<countI; ++i){ var c = components[i]; var componentLabel = c.get('data')['name']; c.bind('click', sendFunc.bind(this, 'Skin', 'click', componentLabel), this); } var items = this.getByClassName('PlayListItem'); var media2Item = {}; for(var i = 0, countI = items.length; i<countI; ++i) { var item = items[i]; var media = item.get('media'); if(!(media.get('id') in media2Item)) { item.bind('begin', sendFunc.bind(this, 'Media', 'play', media.get('label')), this); media2Item[media.get('id')] = item; } } },
  "playAudioList": function(audios){  if(audios.length == 0) return; var currentAudioCount = -1; var currentAudio; var playGlobalAudioFunction = this.playGlobalAudio; var playNext = function(){ if(++currentAudioCount >= audios.length) currentAudioCount = 0; currentAudio = audios[currentAudioCount]; playGlobalAudioFunction(currentAudio, playNext); }; playNext(); },
  "pauseGlobalAudios": function(caller, exclude){  if (window.pauseGlobalAudiosState == undefined) window.pauseGlobalAudiosState = {}; if (window.pauseGlobalAudiosList == undefined) window.pauseGlobalAudiosList = []; if (caller in window.pauseGlobalAudiosState) { return; } var audios = this.getByClassName('Audio').concat(this.getByClassName('VideoPanoramaOverlay')); if (window.currentGlobalAudios != undefined) audios = audios.concat(Object.values(window.currentGlobalAudios)); var audiosPaused = []; var values = Object.values(window.pauseGlobalAudiosState); for (var i = 0, count = values.length; i<count; ++i) { var objAudios = values[i]; for (var j = 0; j<objAudios.length; ++j) { var a = objAudios[j]; if(audiosPaused.indexOf(a) == -1) audiosPaused.push(a); } } window.pauseGlobalAudiosState[caller] = audiosPaused; for (var i = 0, count = audios.length; i < count; ++i) { var a = audios[i]; if (a.get('state') == 'playing' && (exclude == undefined || exclude.indexOf(a) == -1)) { a.pause(); audiosPaused.push(a); } } },
  "syncPlaylists": function(playLists){  var changeToMedia = function(media, playListDispatched){ for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(playList != playListDispatched){ var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ if(items[j].get('media') == media){ if(playList.get('selectedIndex') != j){ playList.set('selectedIndex', j); } break; } } } } }; var changeFunction = function(event){ var playListDispatched = event.source; var selectedIndex = playListDispatched.get('selectedIndex'); if(selectedIndex < 0) return; var media = playListDispatched.get('items')[selectedIndex].get('media'); changeToMedia(media, playListDispatched); }; var mapPlayerChangeFunction = function(event){ var panoramaMapLocation = event.source.get('panoramaMapLocation'); if(panoramaMapLocation){ var map = panoramaMapLocation.get('map'); changeToMedia(map); } }; for(var i = 0, count = playLists.length; i<count; ++i){ playLists[i].bind('change', changeFunction, this); } var mapPlayers = this.getByClassName('MapPlayer'); for(var i = 0, count = mapPlayers.length; i<count; ++i){ mapPlayers[i].bind('panoramaMapLocation_change', mapPlayerChangeFunction, this); } },
  "loopAlbum": function(playList, index){  var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var loopFunction = function(){ player.play(); }; this.executeFunctionWhenChange(playList, index, loopFunction); },
  "existsKey": function(key){  return key in window; },
  "stopAndGoCamera": function(camera, ms){  var sequence = camera.get('initialSequence'); sequence.pause(); var timeoutFunction = function(){ sequence.play(); }; setTimeout(timeoutFunction, ms); },
  "openLink": function(url, name){  if(url == location.href) { return; } var isElectron = (window && window.process && window.process.versions && window.process.versions['electron']) || (navigator && navigator.userAgent && navigator.userAgent.indexOf('Electron') >= 0); if (name == '_blank' && isElectron) { if (url.startsWith('/')) { var r = window.location.href.split('/'); r.pop(); url = r.join('/') + url; } var extension = url.split('.').pop().toLowerCase(); if(extension != 'pdf' || url.startsWith('file://')) { var shell = window.require('electron').shell; shell.openExternal(url); } else { window.open(url, name); } } else if(isElectron && (name == '_top' || name == '_self')) { window.location = url; } else { var newWindow = window.open(url, name); newWindow.focus(); } },
  "playGlobalAudio": function(audio, endCallback){  var endFunction = function(){ audio.unbind('end', endFunction, this); this.stopGlobalAudio(audio); if(endCallback) endCallback(); }; audio = this.getGlobalAudio(audio); var audios = window.currentGlobalAudios; if(!audios){ audios = window.currentGlobalAudios = {}; } audios[audio.get('id')] = audio; if(audio.get('state') == 'playing'){ return audio; } if(!audio.get('loop')){ audio.bind('end', endFunction, this); } audio.play(); return audio; }
 }
};

    
    function HistoryData(playList) {
        this.playList = playList;
        this.list = [];
        this.pointer = -1;
    }

    HistoryData.prototype.add = function(index){
        if(this.pointer < this.list.length && this.list[this.pointer] == index) {
            return;
        }
        ++this.pointer;
        this.list.splice(this.pointer, this.list.length - this.pointer, index);
    };

    HistoryData.prototype.back = function(){
        if(!this.canBack()) return;
        this.playList.set('selectedIndex', this.list[--this.pointer]);
    };

    HistoryData.prototype.forward = function(){
        if(!this.canForward()) return;
        this.playList.set('selectedIndex', this.list[++this.pointer]);
    };

    HistoryData.prototype.canBack = function(){
        return this.pointer > 0;
    };

    HistoryData.prototype.canForward = function(){
        return this.pointer >= 0 && this.pointer < this.list.length-1;
    };
    //

    if(script.data == undefined)
        script.data = {};
    script.data["history"] = {};    //playListID -> HistoryData

    TDV.PlayerAPI.defineScript(script);
})();
